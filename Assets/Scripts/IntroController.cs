﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entitas.Unity;
using Cinemachine;

public class IntroController : MonoBehaviour {

	public float FadeTime;

	private List<SpriteRenderer> _Renderers;
	private Vector3 _StartingPosition;
	private bool _Begun;

	private void Awake()
	{
		_Renderers = new List<SpriteRenderer>();
		GetComponentsInChildren<SpriteRenderer>(_Renderers);
	}

	void Start () {
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space) && !_Begun)
		{
			_Begun = true;
			StartCoroutine(StartRoutine());
		}
	}

	IEnumerator StartRoutine()
	{
		var time = 0f;
		var duration = 3f;
		while (time < duration)
		{
			time += Contexts.sharedInstance.game.time.Tick;
			var easeInOut = 1f - Easing.EaseInOut(time / duration, 2);
			foreach (var renderer in _Renderers)
			{
				var color = renderer.color;
				color.a = easeInOut;
				renderer.color = color;
			}
			yield return 0;
		}
		yield return null;
	}
}
