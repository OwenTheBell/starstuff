﻿using UnityEngine;
using System.Collections.Generic;

namespace Common.ObjectPooling
{
    public class ObjectPoolManager : MonoBehaviour {

        #region Object Pool Data

        private List<ObjectPooler> _pools;
        private Vector3 _poolPosition = new Vector3(0f, 0f, 0f);

        #endregion // Object Pool Data


        #region Singleton

        private static ObjectPoolManager s_instance = null;

        public static ObjectPoolManager Instance {
            get { return s_instance; }
        }

        #endregion // Singleton


        #region Unity Lifecycle Methods

        void Awake() {
            if (!s_instance)
                s_instance = this;
            _pools = new List<ObjectPooler>();
        }

        #endregion // Unity Lifecycle Methods


        #region Public Methods

        /// <summary>
        /// Clear all ObjectPools
        /// </summary>
        public void Empty() {
            _pools.Act(p => {
                p.Empty();
                Destroy(p.gameObject); // the pool must be destroyed to prevent memory leaks
            });
            _pools.Clear();
        }

        /// <summary>
        /// Initialize all pools
        /// </summary>
        public void InitializePools() {
            _pools.Act(p => p.InitObjectPool());
        }

        /// <summary>
        /// Create a new pool for the provided gameobject
        /// </summary>
        /// <param name="gO"> The gameobject to pool. </param>
        /// <returns></returns>
        public int AddToObjectPool(GameObject gO) {
            return AddToObjectPool(gO, gO.name);
        }

        /// <summary>
        /// Create a new pool for the provided GameObject
        /// </summary>
        /// <param name="gO"> The gameobject to pool. </param>
        /// <param name="name"> Name for the object pool. </param>
        /// <returns> The index of the pool that was created. </returns>
        public int AddToObjectPool(GameObject gO, string name) {
            var poolGO = new GameObject();
            poolGO.transform.parent = transform;
            poolGO.transform.localPosition = _poolPosition;
            _poolPosition.x += 5f;

            var pooler = poolGO.AddComponent<ObjectPooler>();
            pooler.poolName = name;
            pooler.blueprint = gO;
            pooler.pooledAmount = 20;
            pooler.willGrow = true;
            pooler.poolIndex = _pools.Count;
            _pools.Add(pooler);
            return pooler.poolIndex;
        }

        /// <summary>
        /// Gets a pooled object from the specific index
        /// </summary>
        /// <param name="index"> Index of the object pooler</param>
        /// <returns> If succesful, returns inactive pooled object. Else, returns null</returns>
        public GameObject GetPooledObject(int index) {
            if (index < _pools.Count) {
                return _pools[index].GetPooledObject();
            }
            return null;
        }

        /// <summary>
        /// Add a GameObject back to it's pool
        /// </summary>
        /// <param name="gO"> GameObject to return </param>
        /// <param name="index"> Index of pool to return the object to </param>
        public void ReturnObjectToPool(GameObject gO, int index) {
            if (index < _pools.Count) {
                _pools[index].ReturnObjectToPool(gO);
            }
        }

        /// <summary>
        /// Get the blueprint GameObject for the pool at index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public GameObject GetBlueprint(int index) {
            if (index < _pools.Count) {
                return _pools[index].blueprint;
            }
            return null;
        }

        /// <summary>
        /// Replace the blueprint GameObject for the pool at index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="gO"></param>
        public void ReplaceBlueprint(int index, GameObject gO) {
            if (index < _pools.Count) {
                _pools[index].blueprint = gO;
            }
        }

        public string GetName(int index) {
            if (index >= 0 && index < _pools.Count) {
                return _pools[index].poolName;
            }
            return string.Empty;
        }

        /// <summary>
        /// Change the name of the pool at index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="name"></param>
        public void ChangeName(int index, string name) {
            if (index < _pools.Count) {
                _pools[index].poolName = name;
            }
        }

        /// <summary>
        /// Change the limit of an object pool at the index. The
        /// limit prevents new objects from being created if the object's 
        /// count exceeds it.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="limit"></param>
        public void SetObjectPoolLimit(int index, int limit) {
            if (index < _pools.Count) {
                _pools[index].pooledLimit = limit;
            }
        }
        #endregion // Public Methods
    }
}