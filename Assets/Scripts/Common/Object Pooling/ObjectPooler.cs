﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Common.ObjectPooling {
    /// <summary>
    /// Pools a number of objects 
    /// </summary>
    public class ObjectPooler : MonoBehaviour {

        #region Inspector Variables

        private GameObject _blueprint;
        /// <summary>
        /// Object to pool.
        /// </summary>
        public GameObject blueprint {
            get { return _blueprint; }
            set {
                Destroy(_blueprint);
                _blueprint = value;
                _blueprint.transform.SetParent(transform);
                _blueprint.transform.localPosition = _blueprintPosition;
                _blueprint.name = poolName + " Blueprint";
            }
        }

        private string _poolName;
        public string poolName {
            get {
                if (_poolName == null) {
                    _poolName = string.Empty;
                }
                return _poolName;
            }
            set {
                _poolName = value;
                gameObject.name = _poolName + " Pool";
                if (blueprint != null) {
                    _blueprint.name = poolName + " Blueprint";
                }
            }
        }
        /// <summary>
        /// Number of objects to pool
        /// </summary>
        public int pooledAmount = 20;
        public int pooledLimit = int.MaxValue;
        /// <summary>
        /// Enable pool to dynamically grow.
        /// </summary>
        public bool willGrow = true;
        public int poolIndex;

        #endregion // Inspector Variables


        #region Private Variables

        private Vector3 _blueprintPosition = new Vector3(10000f, 10000f, 0f);
        private List<GameObject> _pooledObjects;
        private int _nextAvailableIndex;

        #endregion // Private Variables


        #region Accessor Methods

        public List<GameObject> PooledObjects {
            get { return _pooledObjects; }
        }

        #endregion // Accessor Methods


        #region Public Methods

        /// <summary>
        /// Initializes the object pool.
        /// </summary>
        public void InitObjectPool() {
            if (_pooledObjects != null) {
                _pooledObjects.Act(o => Destroy(o));
                _pooledObjects.Clear();
            }
            else {
                _pooledObjects = new List<GameObject>();
            }
            for (int i = 0; i < pooledAmount; i++) {
                GrowPool();
            }
            _nextAvailableIndex = 0;
        }

        /// <summary>
        /// Gets a pooled object. 
        /// </summary>
        /// <returns> Returns an inactive pooled object. </returns>
        public GameObject GetPooledObject() {
            if (_nextAvailableIndex < _pooledObjects.Count) {
                return _pooledObjects[_nextAvailableIndex++];
            }
            if (_pooledObjects.Count+1 > pooledLimit) {
                return null;
            }
            if (willGrow) {
                _nextAvailableIndex++;
                return GrowPool();
            }
            else {
                return null;
            }
        }

        /// <summary>
        /// Empty the pool and destroy all pooled objects. Null the blueprint
        /// </summary>
        public void Empty() {
            _pooledObjects.Act(o => Destroy(o));
            _pooledObjects.Clear();
            blueprint = null;
            _pooledObjects = null;
        }

        /// <summary>
        /// Add a GameObject back to the pool
        /// </summary>
        /// <param name="gO"></param>
        public void ReturnObjectToPool(GameObject gO) {
            gO.transform.SetParent(transform);
            gO.transform.localPosition = Vector3.zero;
            gO.SetActive(false);
            gO.layer = blueprint.layer;
            // reset base values of Rigidbody as these may have been changed during play
            if (blueprint.HasComponent(typeof(Rigidbody))) {
                gO.GetComponent<Rigidbody>().isKinematic = blueprint.GetComponent<Rigidbody>().isKinematic;
            }
            var indexOf = _pooledObjects.IndexOf(gO);
            _pooledObjects[indexOf] = _pooledObjects[_nextAvailableIndex - 1];
            _pooledObjects[_nextAvailableIndex - 1] = gO;
            _nextAvailableIndex--;
        }

        #endregion // Public Methods


        #region Private Methods

        /// <summary>
        /// Initialize a new object in the pool and return it
        /// </summary>
        /// <returns></returns>
        GameObject GrowPool() {
            GameObject obj = (GameObject)Instantiate(blueprint);
            obj.transform.SetParent(transform);
            obj.transform.localPosition = Vector3.zero;
            obj.SetActive(false);
            obj.name = poolName;
            var tag = obj.AddComponent<ObjectPoolTag>();
            tag.FromPool = poolIndex;
            _pooledObjects.Add(obj);
            return obj;
        }

        #endregion // Private Methods
    }
}