﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticThrust : MonoBehaviour, IEntityConfiguration {

	private int _ID;

	public void Configure(Contexts contexts, GameEntity e)
	{
		_ID = e.id.value;
	}
	
	void Update () {
		var input = Services.Instance.GameController.AllContexts.input.CreateEntity();
		input.AddTarget(_ID);
		input.isThrust = true;
	}
}
