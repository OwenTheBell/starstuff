﻿using UnityEngine;
using Entitas;
using Entitas.Unity;
using Common.ObjectPooling;

public interface IView
{
	GameObject GameObject { get; }
	Transform Transform { get; }
	bool Active { get; set; }
	Vector3 Position { get; set; }
	Quaternion Rotation { get; set; }
	Vector3 Scale { get; set; }
	Vector3 RenderScale { get; set; }
	Vector3 DefaultRenderScale { get; }
	bool Visible { get; }
	void Initialize(Contexts contexts, IEntity e);
	void DestroyView();
}

/// <summary>
/// Basic interface between Entitas and a GameObject
/// </summary>
public class ViewController : MonoBehaviour
	, IView
	, IActiveListener
	, IActiveRemovedListener
	, IDestroyingListener
{

	#region IView Properties

	public GameObject GameObject
	{
		get
		{
			return gameObject;
		}
	}

	public Transform Transform
	{
		get
		{
			return transform;
		}
	}

	public bool Active
	{
		get
		{
			return gameObject.activeSelf;
		}
		set
		{
			gameObject.SetActive(value);
		}
	}

	public bool Visible
	{
		get
		{
			var renderer = GetComponentInChildren<Renderer>();
			return renderer != null && renderer.isVisible;
		}
	}

	public Vector3 Position
	{
		get { return transform.position; }
		set { transform.position = value; }
	}

	public Quaternion Rotation
	{
		get { return transform.rotation; }
		set { transform.rotation = value; }
	}

	public Vector3 Scale
	{
		get { return transform.localScale; }
		set { transform.localScale = value; }
	}

	private Renderer _Renderer;
	private Transform _RendererTransform;
	public Vector3 RenderScale
	{
		get { return _RendererTransform.localScale; }
		set { _RendererTransform.localScale = value; }
	}
	public Vector3 DefaultRenderScale { get; private set; }

	#endregion // IView Properties


	#region Data

	private Contexts _Contexts;
	private GameEntity _Entity;

	private int _DefaultLayer;

	#endregion // Data


	#region Unity Lifecycle Methods 

	private void Awake()
	{
		_DefaultLayer = gameObject.layer;
	}

	private void Update()
	{
		if (_Renderer != null)
		{
			_Entity.isVisible = _Renderer.isVisible;
		}
	}

	#endregion // Unity Lifecycle Methods


	#region IView Methods

	/// <summary>
	/// Connect IEntity e with this GameObject via an EntityLink
	/// Also sets up Unity related components and runs all IEntityConfigurations
	/// </summary>
	/// <param name="contexts"></param>
	/// <param name="e"></param>
	public void Initialize(Contexts contexts, IEntity e)
	{
		_Contexts = contexts;
		_Entity = (GameEntity)e;
		_Entity.AddDestroyingListener(this);

		if (!_Entity.hasView)
		{
			_Entity.AddView(this);
		}
		gameObject.Link(_Entity, contexts.game);

		_Renderer = GetComponentInChildren<Renderer>();
		if (_Renderer != null)
		{
			_RendererTransform = _Renderer.transform;
		}
		else
		{
			_RendererTransform = transform;
		}
		DefaultRenderScale = _RendererTransform.localScale;

		// default is active
		_Entity.isActive = true;
		_Entity.AddActiveListener(this);
		_Entity.AddActiveRemovedListener(this);

		foreach (var config in GetComponentsInChildren<IEntityConfiguration>())
		{
			config.Configure(_Contexts, _Entity);
		}
	}

	/// <summary>
	/// Seperate the ViewController from the entity and remove all listeners
	/// If the GameObject is pooled, return it to that pool.
	/// </summary>
	public void DestroyView()
	{
		// it's possible that the entity has been destroyed so double check
		// before removing the reference

		if (_Entity != null)
		{
			foreach (var deconfig in GetComponentsInChildren<IEntityDeconfiguration>())
			{
				deconfig.Deconfigure(_Entity);
			}
			_Entity.RemoveActiveListener(this);
			_Entity.RemoveActiveRemovedListener(this);
			_Entity.RemoveView();
			_Entity = null;
		}
		gameObject.Unlink();
		gameObject.SetActive(true);

		if (gameObject.HasComponent(typeof(ObjectPoolTag)))
		{
			gameObject.layer = _DefaultLayer;
			if (gameObject.HasComponent(typeof(Rigidbody)))
			{
				gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
				gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			}
			var index = gameObject.GetComponent<ObjectPoolTag>().FromPool;
			ObjectPoolManager.Instance.ReturnObjectToPool(gameObject, index);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	#endregion // IView Methods


	#region Component Listener Methods

	public void OnActive(GameEntity e)
	{
		gameObject.SetActive(true);
	}

	public void OnActiveRemoved(GameEntity e)
	{
		gameObject.SetActive(false);
	}

	public void OnDestroying(GameEntity e)
	{
		Destroy(this);
	}

	#endregion // Component Listener Methods
}