﻿public interface IEntityConfiguration {
    void Configure(Contexts contexts, GameEntity e);
}