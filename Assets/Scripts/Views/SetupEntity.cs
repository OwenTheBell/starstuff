﻿using UnityEngine;

public class SetupEntity : MonoBehaviour, ISetupEntity
{

	public GameEntity Setup(Contexts contexts)
	{
		var e = contexts.game.CreateEntity();
		Services.Instance.View.Create(e, gameObject);
		return e;
	}
}