﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;

public partial class Services
{
	public IFriendSpawner FriendSpawner;
}

public interface IFriendSpawner
{
	GameEntity Spawn(Contexts contexts, Vector3 position);
}

public class FriendSpawner : MonoBehaviour
	, IFriendSpawner
	, IVisibleListener
{

	public GameObject Friend;
	public int FirstSpawnCount = 1;
	public int MaxFriends = 10;
	public Vector2 BehaviorLockout;
	public float Cooldown;
	public List<SpecialBehavior> SpecialBehaviors;
	[FoldoutGroup("Game Over")]
	public float GameDuration;
	[FoldoutGroup("Game Over")]
	public SpecialBehavior GameOverBehavior;
	[FoldoutGroup("Game Over")]
	public float CleanupDuration;
	[FoldoutGroup("Game Over")]
	public CreditControl Credits;

	private AudioSource _Source;
	private bool _FirstSpawn = true;
	private bool _MusicPlaying = false;
	private List<AIEntity> _Agents;
	[SerializeField, ReadOnly]
	private float _TimeRemaining;
	private float _NextCleanup = 0f;

	void Awake()
	{
		Services.Instance.FriendSpawner = this;
		_Source = GetComponent<AudioSource>();
		_Agents = new List<AIEntity>();

		Contexts.sharedInstance.aI.SetBehaviorLockoutTime(BehaviorLockout.x, BehaviorLockout.y, 0f);
		Contexts.sharedInstance.aI.SetActingCooldown(Cooldown);
		_TimeRemaining = Mathf.Infinity;
	}

	void Update()
	{
		_NextCleanup -= Contexts.sharedInstance.game.time.Tick;
		if (_TimeRemaining <= 10)
		{
			Contexts.sharedInstance.aI.behaviorLockoutTime.remaining = 10000000f;
		}
		if (_TimeRemaining >= 0f)
		{
			_TimeRemaining -= Contexts.sharedInstance.game.time.Tick;
		}
		else if (_NextCleanup <= 0f)
		{
			var nonactors = _Agents.Where(e => !e.isActing).ToArray();
			if (nonactors.Length > 0)
			{
				var actor = nonactors[Random.Range(0, nonactors.Length)];
				var gameOverTree = GameOverBehavior.GenerateTree(Contexts.sharedInstance, actor);
				gameOverTree.Begin();
				actor.ReplaceBehavior(gameOverTree);
				_Agents.Remove(actor);
				_NextCleanup = CleanupDuration/MaxFriends;
			}
			if (_Agents.Count == 0)
			{
				Credits.ShowCredits();
				_NextCleanup = 1000000000f;
			}
		}
	}

	public GameEntity Spawn(Contexts contexts, Vector3 position)
	{
		if (_Agents.Count >= MaxFriends || _TimeRemaining <= 0f)
		{
			return null;
		}
		var friend = contexts.game.CreateEntity();
		friend.AddVisibleListener(this);
		var gameObject = Instantiate(Friend, position, Quaternion.identity);
		Services.Instance.View.Create(friend, gameObject);
		gameObject.name = "Friend " + friend.id.value;

		var agent = contexts.aI.GetEntityWithId(friend.aIAgent.id);
		SetAgentSpecialBehaviors(agent);
		_Agents.Add(agent);

		if (_FirstSpawn && FirstSpawnCount > 1)
		{
			_FirstSpawn = false;
			for (var i = 1; i < FirstSpawnCount; i++)
			{
				Spawn(contexts, position);
			}
		}
		return friend;
	}

	public void OnVisible(GameEntity e)
	{
		if (!_MusicPlaying)
		{
			_TimeRemaining = GameDuration;
			_Source.Play();
			_MusicPlaying = true;
		}
	}

	[Button]
	public void UpdateBehaviors()
	{
		var lockout = Contexts.sharedInstance.aI.behaviorLockoutTime;
		lockout.min = BehaviorLockout.x;
		lockout.max = BehaviorLockout.y;
		lockout.remaining = Random.Range(lockout.min, lockout.max);
		Contexts.sharedInstance.aI.ReplaceActingCooldown(Cooldown);

		foreach (var agent in _Agents)
		{
			SetAgentSpecialBehaviors(agent);
		}
	}

	void SetAgentSpecialBehaviors(AIEntity agent)
	{
		var specialBehaviors = new List<BehaviorNode>();
		foreach (var behavior in SpecialBehaviors)
		{
			specialBehaviors.Add(behavior.GenerateTree(Contexts.sharedInstance, agent));
		}
		agent.behaviorManager.value.AddBehaviors(specialBehaviors);
	}
}