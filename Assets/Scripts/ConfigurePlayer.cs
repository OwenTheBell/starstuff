﻿using UnityEngine;

public class ConfigurePlayer : MonoBehaviour
	, IEntityConfiguration
	, IGameAccelerateListener
	, IGameAccelerateRemovedListener
{
	public float AcclerationAdjust;
	public float MaximumVelocityAdjust;
	public bool StartLockedOut;

	private Animator _Animator;

	public void Configure(Contexts contexts, GameEntity e)
	{
		e.isPlayer = true;
		e.AddMovementAdjust(AcclerationAdjust, MaximumVelocityAdjust);

		e.AddGameAccelerateListener(this);
		e.AddGameAccelerateRemovedListener(this);

		_Animator = GetComponent<Animator>();

		e.isLockoutControl = StartLockedOut;
	}

	public void OnAccelerate(GameEntity e, Vector3 value)
	{
		_Animator.SetBool("Accelerating", true);
	}

	public void OnAccelerateRemoved(GameEntity e)
	{
		_Animator.SetBool("Accelerating", false);
	}
}