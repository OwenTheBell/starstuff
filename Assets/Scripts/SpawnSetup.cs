﻿using UnityEngine;

public class SpawnSetup : MonoBehaviour, IEntityConfiguration
{

	public float FirstSpawn;
	public float Interval;

	public void Configure(Contexts contexts, GameEntity e)
	{
		e.AddSpawnInterval(FirstSpawn, Interval);
	}
}
