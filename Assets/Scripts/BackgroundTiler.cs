﻿using System.Collections.Generic;
using UnityEngine;

public class BackgroundTiler : MonoBehaviour
{

	public Transform Player;
	public int TileSize;
	public int GridExtent;
	public float Density;
	public float Scale;
	[Range(0, 1)]
	public float Alpha;
	public GameObject TilePrefab;

	private List<Transform> _Tiles;

	private void Awake()
	{
		_Tiles = new List<Transform>();
	}

	private void Start()
	{
		for (var x = -GridExtent; x <= GridExtent; x++)
		{
			for (var y = -GridExtent; y <= GridExtent; y++)
			{
				var tile = Instantiate(TilePrefab, new Vector2(x, y) * TileSize, Quaternion.identity);
				tile.transform.parent = transform;
				tile.GetComponent<Tile>().Dimensions = Vector2.one * TileSize;
				tile.GetComponent<Tile>().Density = Density;
				tile.GetComponent<Tile>().Scale = Scale;
				tile.GetComponent<Tile>().Alpha = Alpha;
				_Tiles.Add(tile.transform);
			}
		}
	}

	void Update()
	{
		foreach (var tile in _Tiles)
		{
			var position = tile.position;
			if (Mathf.Abs(position.x - Player.position.x) > GridExtent * TileSize)
			{
				if (position.x > Player.position.x)
				{
					position.x -= GridExtent * 2 * TileSize;
				}
				else
				{
					position.x += GridExtent * 2 * TileSize;
				}
			}
			if (Mathf.Abs(position.y - Player.position.y) > GridExtent * TileSize)
			{
				if (position.y > Player.position.y)
				{
					position.y -= GridExtent * 2 * TileSize;
				}
				else
				{
					position.y += GridExtent * 2 * TileSize;
				}
			}
			tile.position = position;
		}
	}
}
