﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class DebugUtils
{

	public static void Log(string value)
	{
#if UNITY_EDITOR
		Debug.Log(value);
#endif
	}
}
