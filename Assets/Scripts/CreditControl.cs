﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditControl : MonoBehaviour
{

	public float Delay;
	public float FadeTime;

	private List<SpriteRenderer> _Renderers;

	private void Awake()
	{
		_Renderers = new List<SpriteRenderer>();
		GetComponentsInChildren<SpriteRenderer>(_Renderers);
			foreach (var renderer in _Renderers)
			{
				var color = renderer.color;
				color.a = 0f;
				renderer.color = color;
			}
	}

	public void ShowCredits()
	{
		StartCoroutine(Show());
	}

	IEnumerator Show()
	{
		yield return new WaitForSeconds(Delay);
		var time = 0f;
		while (time < FadeTime)
		{
			time += Contexts.sharedInstance.game.time.Tick;
			var easeInOut = Easing.EaseInOut(time / FadeTime, 2);
			foreach (var renderer in _Renderers)
			{
				var color = renderer.color;
				color.a = easeInOut;
				renderer.color = color;
			}
			yield return 0;
		}
		yield return null;
	}
}
