﻿using UnityEngine;
using Entitas.Unity;

public class RepulseOnCollision : MonoBehaviour {

	public Vector2 Force;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		var other = collision.gameObject;
		if (other.HasComponent(typeof(EntityLink)))
		{
			var entity = other.GetComponent<EntityLink>().entity as GameEntity;
			var velocity = entity.velocity.value;
			velocity.x *= Force.x;
			velocity.y *= Force.y;
			entity.ReplaceVelocity(velocity);
			//var input = Contexts.sharedInstance.input.CreateEntity();
			//input.AddTarget((other.GetComponent<EntityLink>().entity as GameEntity).id.value);
			//input.AddAccelerate(Force * 10f);
		}
	}
}
