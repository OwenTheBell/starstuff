﻿using UnityEngine;

public class TurnWithPlayer : MonoBehaviour
	, IEntityConfiguration
	, IGameTurnListener
	, IGameTurnRemovedListener
{

	public Vector2 ResponseDelay;

	private TimeComponent _Time;
	private int _Direction = 0;
	private float _Velocity;

	private float _Timer;

	private GameEntity _Player;

	void Start()
	{
		transform.Rotate(Vector3.forward, Random.Range(0, 360));
	}

	void Update()
	{
		if (_Player == null)
		{
			return;
		}

		if (_Timer > 0f)
		{
			_Timer -= _Time.Tick;
		}
		else if (_Direction != 0)
		{
			if (Mathf.Abs(_Velocity) < _Player.maximumAngularVelocity.value)
			{
				_Velocity += _Player.angularAcceleration.value * _Direction * _Time.Tick;
			}
		}
		else
		{
			var impulse = _Player.angularAcceleration.value * _Time.Tick;
			if (impulse > Mathf.Abs(_Velocity))
			{
				_Velocity = 0f;
			}
			else
			{
				_Velocity -= impulse * (_Velocity / Mathf.Abs(_Velocity));
			}
		}

		if (Mathf.Abs(_Velocity) > 0f)
		{
			transform.Rotate(Vector3.forward, _Velocity * _Time.Tick);
		}
	}

	public void Configure(Contexts contexts, GameEntity e)
	{
		_Time = contexts.game.time;
		e.AddGameTurnListener(this);
		e.AddGameTurnRemovedListener(this);

		_Direction = 0;
	}

	public void OnTurn(GameEntity e, float direction)
	{
		var intDirection = (direction > 0f) ? 1 : -1;
		if (intDirection != _Direction)
		{
			PrepareTurn(e, intDirection);
		}
	}

	public void OnTurnRemoved(GameEntity e)
	{
		PrepareTurn(e, 0);
	}

	void PrepareTurn(GameEntity e, int direction)
	{
		_Player = e;
		_Timer = Random.Range(ResponseDelay.x, ResponseDelay.y);
		_Direction = direction;
	}
}
