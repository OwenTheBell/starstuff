﻿using UnityEngine;
using Cinemachine;
using Sirenix.OdinInspector;

public class ScaleCameraSizeWithVelocityListener : MonoBehaviour
	, IEntityConfiguration
	, IVelocityListener
	, IFollowingListener
{

	public CinemachineVirtualCamera Camera;
	public float ScalePerFollower;
	[Range(0f, 2f)]
	public float ZoomInPercentPerSecond;
	[Range(0f, 2f)]
	public float ZoomOutPercentPerSecond;

	[SerializeField, ReadOnly]
	private Vector2 _ScaleRange;
	private float _Velocity;
	private float _MaxVelocity;

	void Awake()
	{
		_ScaleRange = new Vector2(Camera.m_Lens.OrthographicSize, Camera.m_Lens.OrthographicSize - 1);
	}

	void Update()
	{
		var cameraSize = Camera.m_Lens.OrthographicSize;
		var velocityPercent = _Velocity / _MaxVelocity;
		var camPercent = 1f - (cameraSize - _ScaleRange.y) / (_ScaleRange.x - _ScaleRange.y);
		var diff = velocityPercent - camPercent;
		if (diff < 0f)
			diff *= ZoomOutPercentPerSecond * Time.deltaTime;
		if (diff > 0f)
		{
			diff *= ZoomInPercentPerSecond * Time.deltaTime;
		}
		Camera.m_Lens.OrthographicSize = Mathf.Lerp(_ScaleRange.x, _ScaleRange.y, camPercent + diff);
	}

	public void Configure(Contexts contexts, GameEntity e)
	{
		e.AddVelocityListener(this);
		e.AddFollowingListener(this);
	}

	public void OnFollowing(GameEntity e)
	{
		_ScaleRange.y -= ScalePerFollower;
	}

	public void OnVelocity(GameEntity e, Vector3 velocity)
	{
		_Velocity = velocity.magnitude;
		_MaxVelocity = e.maximumVelocity.value;
	}
}
