﻿using UnityEngine;

public class TurnWithFacingListener : MonoBehaviour
	, IEntityConfiguration
	, IFacingListener
{

	public void Configure(Contexts contexts, GameEntity e)
	{
		e.AddFacingListener(this);
	}

	public void OnFacing(GameEntity e, float value)
	{
		var rotation = transform.eulerAngles;
		rotation.z = value;
		transform.eulerAngles = rotation;
	}


}
