﻿using UnityEngine;
using Rewired;
using Sirenix.OdinInspector;

public class ThrustInputIssuer : MonoBehaviour, IEntityConfiguration
{

	public int PlayerIndex;
	public float Acceleration;
	public float MaximumVelocity;
	[PropertyRange(0, 1)]
	public float Damping;

	private int _ID;

	public void Configure(Contexts contexts, GameEntity e)
	{
		_ID = e.id.value;

		e.AddAcceleration(Acceleration);
		e.AddVelocity(Vector3.zero);
		e.AddMaximumVelocity(MaximumVelocity);
		e.AddVelocityDamping(Damping);
	}

	void Update()
	{
		var player = ReInput.players.GetPlayer(PlayerIndex);
		if (player.GetButton(RewiredConsts.Action.Accelerate))
		{
			var input = Services.Instance.GameController.AllContexts.input.CreateEntity();
			input.AddTarget(_ID);
			input.isThrust = true;
		}

	}
}