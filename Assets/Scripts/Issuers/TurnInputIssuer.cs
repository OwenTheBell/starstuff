﻿using UnityEngine;
using Rewired;

public class TurnInputIssuer : MonoBehaviour, IEntityConfiguration
{

	public int PlayerIndex;
	public float AngularAcceleration;
	public float MaximumAngularVelocity;
	public float Damping;

	private int _ID;

	public void Configure(Contexts contexts, GameEntity e)
	{
		_ID = e.id.value;

		e.AddAngularAcceleration(AngularAcceleration);
		e.AddAngularVelocity(0f);
		e.AddFacing(transform.eulerAngles.z);
		e.AddMaximumAngularVelocity(MaximumAngularVelocity);
		e.AddAngularVelocityDamping(Damping);
	}

	void Update()
	{
		var player = ReInput.players.GetPlayer(PlayerIndex);
		var turn = player.GetAxis(RewiredConsts.Action.Turn);

		if (turn != 0f)
		{
			var input = Services.Instance.GameController.AllContexts.input.CreateEntity();
			input.AddTarget(_ID);
			input.AddTurn(turn * -1);
		}
	}
}