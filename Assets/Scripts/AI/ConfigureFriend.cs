﻿using UnityEngine;

public class ConfigureFriend : MonoBehaviour, IEntityConfiguration
{

	public float Acceleration;
	public float MaximumVelocity;
	public float AccelerationScale;
	public float MaximumVelocityScale;
	public Sprite[] Sprites;

	public void Configure(Contexts contexts, GameEntity e)
	{
		e.isFriend = true;
		e.AddAcceleration(Acceleration);
		e.AddMaximumVelocity(MaximumVelocity);
		e.AddVelocity(Vector3.zero);
		e.AddScalingAcceleration(AccelerationScale);
		e.AddScalingMaximumVelocity(MaximumVelocityScale);
		e.isFollowing = true;

		var rads = Random.Range(0f, Mathf.PI * 2);
		var direction = new Vector2(Mathf.Cos(rads), Mathf.Sin(rads));
		e.ReplaceVelocity(direction * MaximumVelocity);

		var renderer = GetComponentInChildren<SpriteRenderer>();
		renderer.sprite = Sprites[Random.Range(0, Sprites.Length)];
	}
}