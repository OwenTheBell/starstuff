﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CirclePointBehaviorTree : MonoBehaviour
	, IEntityConfiguration
	, IEntityDeconfiguration
{

	public Vector2 FriendRepelRange;
	public float FriendRepelForce;
	public Vector2 PlayerRepelRange;
	public float PlayerRepelForce;
	public Vector2 AttractRange;
	public float AttractForce;
	public Vector2 NoiseDelay;
	public float NoiseDuration;
	public float NoiseRadians;
	public float NoiseAmp;

	private AIEntity _AgentEntity;
	private BehaviorNode _Node;

	public void Configure(Contexts contexts, GameEntity e)
	{
		_AgentEntity = contexts.aI.CreateEntity();
		e.AddAIAgent(_AgentEntity.id.value);
		_AgentEntity.AddAgent(e.id.value);
		_AgentEntity.isActing = true;

		var playerMatcher = GameMatcher
			.AllOf(
				GameMatcher.Player
				, GameMatcher.View
				);
		var friendMatcher = GameMatcher
			.AllOf(
				GameMatcher.Friend
				, GameMatcher.View
				)
			.NoneOf(
				GameMatcher.Acting
				);

		_Node = new SimultaneousComposite(contexts, _AgentEntity, new List<BehaviorNode>() {
			new MatchTargetVelocityLeaf(contexts, _AgentEntity
				, playerMatcher
				, new Vector2(10f, 20f)
				, 1
				)
			, new MatchTargetVelocityLeaf(contexts, _AgentEntity
				, friendMatcher
				, new Vector2(10f, 10f)
				, 1
				)
			, new RepelFromLeaf(contexts, _AgentEntity
				, FriendRepelRange
				, FriendRepelForce
				, friendMatcher
				)
			, new RepelFromLeaf(contexts, _AgentEntity
				, PlayerRepelRange
				, PlayerRepelForce
				, playerMatcher
				)
			, new AttractToGroupLeaf(contexts, _AgentEntity
				, playerMatcher
				, AttractRange
				, AttractForce
				)
			// this provides some noise in the entity's movement
			, new LoopUntilDecorator(contexts, _AgentEntity
				, new SequenceNode(contexts, _AgentEntity, new List<BehaviorNode>() {
					new RandomDelayLeaf(contexts, _AgentEntity, NoiseDelay)
					, new RandomizedAccelerationLeaf(contexts, _AgentEntity
						, NoiseDuration
						, NoiseRadians
						, NoiseAmp
						)
					})
				, BehaviorState.Failure
				)
		});

		_AgentEntity.AddBehavior(_Node);
		_Node.Begin();
	}

	public void Deconfigure(GameEntity e)
	{
		_AgentEntity.isDestroy = true;
	}

	void Update()
	{
		var state = _AgentEntity.behavior.value.Process();
	}
}