﻿using System.Collections.Generic;

/// <summary>
/// Works exactly like SelectorComposite except that the order of _Children is
/// randomized each time that it runs
/// </summary>
public class RandomizedSelectorComposite : SelectorComposite
{

	public RandomizedSelectorComposite(
		Contexts contexts
		, AIEntity e
		, List<BehaviorNode> children) : base(contexts, e, children) { }

	public override void Begin()
	{
		for (var i = 0; i < _Children.Count; i++)
		{
			var j = _Children.GetRandomIndex();
			var childI = _Children[i];
			var childJ = _Children[j];
			_Children[j] = childI;
			_Children[i] = childJ;
		}
		base.Begin();
	}
}
