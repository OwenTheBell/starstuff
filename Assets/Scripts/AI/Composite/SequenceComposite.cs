﻿using System.Collections.Generic;

public class SequenceNode : CompositeNode
{

	private int _ActiveIndex;

	public SequenceNode(
		Contexts contexts
		, AIEntity e
		, List<BehaviorNode> children) : base(contexts, e, children) { }

	public override void Begin()
	{
		base.Begin();
		_ActiveIndex = 0;
		_Children[_ActiveIndex].Begin();
	}

	public override BehaviorState Process()
	{
		var state = _Children[_ActiveIndex].Process();
		if (state == BehaviorState.Failure)
		{
			return BehaviorState.Failure;
		}
		else if (state == BehaviorState.Success)
		{
			if (_ActiveIndex == _Children.Count - 1)
			{
				return BehaviorState.Success;
			}
			else
			{
				_ActiveIndex++;
				_Children[_ActiveIndex].Begin();
				// call Process() again to immediately evaluate the new node and
				// see if it's necessary to move on
				return Process();
			}
		}
		return BehaviorState.Running;
	}

	public override void Teardown()
	{
		_Children.Act(c => c.Teardown());
	}
}