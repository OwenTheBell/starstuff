﻿using System.Collections.Generic;

/// <summary>
/// Iterate over all children in linear order until once of the children returns
/// BehaviorState.Success
/// </summary>
public class SelectorComposite : CompositeNode
{

	int _Index;

	public SelectorComposite(
		Contexts contexts
		, AIEntity e
		, List<BehaviorNode> children) : base(contexts, e, children) { }

	public override void Begin()
	{
		base.Begin();
		if (_Children.Count > 0)
		{
			_Index = 0;
			_Children[_Index].Begin();
		}
	}

	public override BehaviorState Process()
	{
		if (_Children.Count == 0)
		{
			return BehaviorState.Failure;
		}
		var state = _Children[_Index].Process();
		if (state == BehaviorState.Failure)
		{
			_Index++;
			if (_Index >= _Children.Count)
			{
				return BehaviorState.Failure;
			}
			else
			{
				_Children[_Index].Begin();
				Process();
			}
		}
		return state;
	}
}