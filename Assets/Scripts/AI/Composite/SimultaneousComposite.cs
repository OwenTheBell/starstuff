﻿using System.Collections.Generic;

/// <summary>
/// Process all Children simulatenously.
/// Each time a child succeeds, stop processing it.
/// Once all children have succeeded, or any fail, end processing.
/// </summary>
public class SimultaneousComposite : CompositeNode 
{

	private List<BehaviorNode> _RunningNodes;

	public SimultaneousComposite(Contexts contexts, AIEntity e, List<BehaviorNode> children) : base(contexts, e, children) { }

	public override void Begin()
	{
		_Children.Act(c => c.Begin());
		_RunningNodes = new List<BehaviorNode>(_Children);
	}

	public override BehaviorState Process()
	{
		var state = BehaviorState.Running;
		for (var i = _RunningNodes.Count - 1; i >= 0; i--)
		{
			var subState = _RunningNodes[i].Process();
			if (state == BehaviorState.Success)
			{
				_RunningNodes.RemoveAt(i);
			}
			else if (state == BehaviorState.Failure)
			{
				return BehaviorState.Failure;
			}
		}
		if (_RunningNodes.Count == 0)
		{
			return BehaviorState.Success;
		}
		return BehaviorState.Running;
	}
}