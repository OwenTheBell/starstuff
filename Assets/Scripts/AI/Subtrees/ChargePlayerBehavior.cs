﻿using System.Collections.Generic;
using UnityEngine;	

namespace ChargePlayer
{
	[CreateAssetMenu(
		fileName = "Charge Player Behavior"
		, menuName = "Starstuff/Behaviors/Charge Player")]
	public class ChargePlayerBehavior : SpecialBehavior
	{

		public Info Info;

		public override BehaviorNode GenerateTree(Contexts contexts, AIEntity e)
		{
			var duration = Random.Range(Info.Duration.x, Info.Duration.y);
			var matcher = GameMatcher
				.AllOf(
					GameMatcher.Player
					, GameMatcher.View);
			var sequence = new SequenceNode(contexts, e
				, new List<BehaviorNode>() {
					new RequireFriendCountLeaf(contexts, e, Info.MinimumFriendsToStart)
					, new SetEntityToActingLeaf(contexts, e)
					, new SetPlayerAsTargetLeaf(contexts, e)
					, new SetBehaviorLockoutLeaf(contexts, e, 0.25f)
					, new TerminateOnDistanceToTargetDecorator(contexts, e
						, new RepelFromLeaf(contexts, e
							, Info.RepelRange
							, Info.RepelAmp
							, matcher
							)
						, Info.RepelRange.y - 1
						, false
						, BehaviorState.Success
						)
					, new ApplyAccelerationAmpLeaf(contexts, e, Info.ChargeAmp)
					, new TerminateOnDurationDecorator(contexts, e
						, new CircleTargetLeaf(contexts, e, 1)
						, duration
						, BehaviorState.Success
						)
					, new RemoveAccelerationAmpLeaf(contexts, e)
					}
				);
			sequence.Name = "Charge Player";
			return sequence;
		}
	}

	[System.Serializable]
	public struct Info
	{
		public int MinimumFriendsToStart;
		public Vector2 Duration;
		public Vector2 RepelRange;
		public float RepelAmp;
		public float ChargeAmp;
	}
}
