﻿using System.Collections.Generic;
using UnityEngine;

namespace CirclePlayer
{
	[CreateAssetMenu(
		fileName = "Circle Player Behavior"
		, menuName = "Starstuff/Behaviors/Circle Player")]
	public class CirclePlayerBehavior : SpecialBehavior
	{

		public CirclingInfo Info;

		public override BehaviorNode GenerateTree(Contexts contexts, AIEntity e)
		{
			var duration = Random.Range(Info.Duration.x, Info.Duration.y);
			var sequence = new SequenceNode(contexts, e
				, new List<BehaviorNode>() {
					new FindAvailableFriendsLeaf(contexts, e, Info.MinFriends, Info.MaxFriends)
					, new SetPlayerAsTargetLeaf(contexts, e)
					, new SetBehaviorLockoutLeaf(contexts, e)
					, new SetupBehaviorLeaf(contexts, e, Info, duration)
					}
				);
			sequence.Name = "Circle Player Setup";
			return sequence;
		}
	}

	[System.Serializable]
	public struct CirclingInfo
	{
		public int MinFriends;
		public int MaxFriends;
		public Vector2 Duration;
		public float AccelerationAmp;
		public float RepelAmp;
		public Vector2 RepelRange;
		public float FollowerAmp;
	}

	public class SetupBehaviorLeaf : LeafNode
	{

		readonly CirclingInfo _Info;
		readonly float _Duration;

		public SetupBehaviorLeaf(Contexts contexts, AIEntity e
			, CirclingInfo info
			, float duration) : base(contexts, e)
		{
			_Info = info;
			_Duration = duration;
		}

		public override BehaviorState Process()
		{
			var direction = (Random.Range(0, 2) == 1) ? 1f : -1f;
			var playerMatcher = GameMatcher
				.AllOf(
					GameMatcher.Player
					, GameMatcher.View
					);
			var masterSequence = new SequenceNode(_Contexts, _Entity
				, new List<BehaviorNode>() {
					new SetEntityToActingLeaf(_Contexts, _Entity)
					, new ApplyAccelerationAmpLeaf(_Contexts, _Entity, _Info.AccelerationAmp)
					, new TerminateOnDurationDecorator(_Contexts, _Entity
						, new SimultaneousComposite(_Contexts, _Entity
							, new List<BehaviorNode>() {
								new CircleTargetLeaf(_Contexts, _Entity, direction)
								, new RepelFromLeaf(_Contexts, _Entity
									, _Info.RepelRange
									, _Info.RepelAmp
									, playerMatcher
									)
								}
							)
						, _Duration
						, BehaviorState.Success
						)
					, new RemoveAccelerationAmpLeaf(_Contexts, _Entity)
					}
				);
			masterSequence.Name = "Circle Player";
			masterSequence.Begin();
			_Entity.ReplaceBehavior(masterSequence);
			var repelList = new List<int>() { _Entity.agent.id };
			for (var i = 0; i < _Entity.trackedFriends.ids.Count; i++) 
			{
				var agent = _Contexts.aI.GetEntityWithId(_Entity.trackedFriends.ids[i]);
				var targetID = _Entity.agent.id;
				if (i > 0)
				{
					var targetAgent = _Contexts.aI.GetEntityWithId(_Entity.trackedFriends.ids[i - 1]);
					targetID = targetAgent.agent.id;
				}
				if (agent.hasTarget)
				{
					agent.RemoveTarget();
				}
				agent.AddTarget(targetID);

				var sequence = new SequenceNode(_Contexts, agent
					, new List<BehaviorNode>() {
						new SetEntityToActingLeaf(_Contexts, agent)
						, new ApplyAccelerationAmpLeaf(_Contexts, agent, _Info.FollowerAmp)
						, new TerminateOnDurationDecorator(_Contexts, agent
							, new SimultaneousComposite(_Contexts, agent
								, new List<BehaviorNode>() {
									new ChaseTargetLeaf(_Contexts, agent, _Info.RepelRange.y)
									, new RepelFromListLeaf(_Contexts, agent
										, _Info.RepelRange
										, _Info.RepelAmp
										, repelList
										)
									}
								)
							, _Duration
							, BehaviorState.Success)
						, new RemoveAccelerationAmpLeaf(_Contexts, agent)
						}
					);
				sequence.Name = "Circle Player";
				sequence.Begin();
				agent.ReplaceBehavior(sequence);
			}
			return BehaviorState.Running;
		}
	}
}