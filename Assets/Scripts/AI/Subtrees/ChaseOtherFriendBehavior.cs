﻿using System.Collections.Generic;
using UnityEngine;

namespace ChaseotherFriend
{
	[CreateAssetMenu(
		fileName = "Chase Other Friend Behavior"
		, menuName = "Starstuff/Behaviors/Chase Other Friend")]
	public class ChaseOtherFriendBehavior : SpecialBehavior
	{

		public Vector2 Duration;
		public float AccelerationAmp;
		public float RepelForce;
		public Vector2 RepelRange;

		public override BehaviorNode GenerateTree(Contexts contexts, AIEntity e)
		{
			var friendMatcher = GameMatcher
				.AllOf(
					GameMatcher.Friend
					, GameMatcher.View);
			var duration = Random.Range(Duration.x, Duration.y);
			var sequence = new SequenceNode(contexts, e
				, new List<BehaviorNode>() {
					new SetRandomFriendAsTargetLeaf(contexts, e)
					, new SetEntityToActingLeaf(contexts, e)
					, new ApplyAccelerationAmpLeaf(contexts, e, AccelerationAmp)
					, new TerminateOnDurationDecorator(contexts, e
						, new SimultaneousComposite(contexts, e
							, new List<BehaviorNode>() {
								new ChaseTargetLeaf(contexts, e, RepelRange.y)
								, new RepelFromLeaf(contexts, e
									, RepelRange
									, RepelForce
									, friendMatcher
									)
								}
							)
						, duration
						, BehaviorState.Success
						)
					, new RemoveAccelerationAmpLeaf(contexts, e)
					}
				);
			sequence.Name = "Chase Other Friend";
			sequence.Begin();
			return sequence;
		}
	}
}