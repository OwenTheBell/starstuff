﻿using System.Collections.Generic;
using UnityEngine;

namespace CircleOtherFriends
{
	[CreateAssetMenu(
		fileName = "Circling Other Friends Behavior"
		, menuName = "Starstuff/Behaviors/Circle Other Friends")]
	public class CircleOtherFriendsBehavior : SpecialBehavior
	{

		public CirclingInfo BehaviorInfo;

		public override BehaviorNode GenerateTree(Contexts contexts, AIEntity e)
		{
			var seqeunce = new SequenceNode(contexts, e
				, new List<BehaviorNode>() {
					new FindAvailableFriendsLeaf(contexts, e, BehaviorInfo.MinFriends, BehaviorInfo.MaxFriends)
					, new GetCommonPointBetweenTargetFriendsLeaf(contexts, e)
					, new SetBehaviorLockoutLeaf(contexts, e)
					, new SetupCircleBehaviorLeaf(contexts, e, BehaviorInfo)
					}
				);
			seqeunce.Name = "Circle Other Friends Setup";
			return seqeunce;
		}
	}

	[System.Serializable]
	public struct CirclingInfo
	{
		public GameObject Prefab;
		public Vector2 Duration;
		public int MinFriends;
		public int MaxFriends;
		public float AccelerationAmp;
		public Vector2 RepelRange;
		public float RepelAmp;
	}

	public class SetupCircleBehaviorLeaf : LeafNode
	{

		readonly CirclingInfo _Info;

		public SetupCircleBehaviorLeaf(
			Contexts contexts
			, AIEntity e
			, CirclingInfo info) : base(contexts, e)
		{
			_Info = info;
		}

		public override BehaviorState Process()
		{
			if (!_Entity.hasTargetPosition)
			{
				return BehaviorState.Failure;
			}

			var agentEntity = _Contexts.game.GetEntityWithId(_Entity.agent.id);
			var targetEntity = _Contexts.game.CreateEntity();
			var targetGO = GameObject.Instantiate(_Info.Prefab, _Entity.targetPosition.value, Quaternion.identity);
			Services.Instance.View.Create(targetEntity, targetGO);
			targetEntity.AddAcceleration(agentEntity.acceleration.value);
			targetEntity.AddMaximumVelocity(agentEntity.maximumVelocity.value);
			targetEntity.AddVelocity(agentEntity.velocity.value);
			targetEntity.AddScalingAcceleration(agentEntity.scalingAcceleration.percent);
			targetEntity.AddScalingMaximumVelocity(agentEntity.scalingMaximumVelocity.percent);

			var direction = (Random.Range(0, 2) == 1) ? 1f : -1f;
			var duration = Random.Range(_Info.Duration.x, _Info.Duration.y);
			var others = new List<int>(_Entity.trackedFriends.ids.Count + 2);
			others.Add(targetEntity.id.value);
			others.Add(agentEntity.id.value);
			foreach (var id in _Entity.trackedFriends.ids)
			{
				var agent = _Contexts.aI.GetEntityWithId(id);
				others.Add(agent.agent.id);
			}
			SetToCircling(_Entity, targetEntity.id.value, direction, duration, others);
			foreach (var id in _Entity.trackedFriends.ids)
			{
				var agent = _Contexts.aI.GetEntityWithId(id);
				SetToCircling(agent, targetEntity.id.value, direction, duration, others);
			}

			// returning Running and not Success because we want the new behavior tree
			// to continue. If you return Success, BehaviorTreeManager will automatically
			// remove the agent from the new state
			return BehaviorState.Running;
		}

		void SetToCircling(AIEntity agent, int targetID, float direction, float duration, List<int> others)
		{
			if (agent.hasTarget)
			{
				agent.RemoveTarget();
			}
			agent.AddTarget(targetID);
			var sequence = new SequenceNode(_Contexts, agent
				, new List<BehaviorNode>(new BehaviorNode[] {
					new SetEntityToActingLeaf(_Contexts, agent)
					, new ApplyAccelerationAmpLeaf(_Contexts, agent, _Info.AccelerationAmp)
					, new TerminateOnDurationDecorator(_Contexts, agent
						, new SimultaneousComposite(_Contexts, agent
							, new List<BehaviorNode>() {
								new RepelFromListLeaf(_Contexts, agent
									, _Info.RepelRange
									, _Info.RepelAmp
									, others
									)
								, new CircleTargetLeaf(_Contexts, agent, direction)
								})
							, duration
							, BehaviorState.Success
							)
					, new RemoveAccelerationAmpLeaf(_Contexts, agent)
					, new DestroyTargetLeaf(_Contexts, agent)
					})
				);
			sequence.Name = "Circle Other Friends";
			agent.ReplaceBehavior(sequence);
			agent.behavior.value.Begin();
		}
	}
}