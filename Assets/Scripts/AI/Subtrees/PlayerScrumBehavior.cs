﻿using System.Collections.Generic;
using UnityEngine;

namespace PlayerScrum
{
	[CreateAssetMenu(
		fileName = "Player Scrum Behavior"
		, menuName = "Starstuff/Behaviors/Player Scrum")]
	public class PlayerScrumBehavior : SpecialBehavior
	{

		public Info Info;

		public override BehaviorNode GenerateTree(Contexts contexts, AIEntity e)
		{
			var duration = Random.Range(Info.MinFriends, Info.MaxFriends);
			var sequence = new SequenceNode(contexts, e
				, new List<BehaviorNode>() {
					new FindAvailableFriendsLeaf(contexts, e, Info.MinFriends, Info.MaxFriends)
					, new SetBehaviorLockoutLeaf(contexts, e)
					, new SetupBehaviorLeaf(contexts, e, Info, duration)
				});
			sequence.Name = "Player Scrum Setup";
			return sequence;
		}

	}

	[System.Serializable]
	public struct Info
	{
		public int MinFriends;
		public int MaxFriends;
		public Vector2 Duration;
		public float AccelerationAmp;
		public float RepelAmp;
		public Vector2 RepelRange;
	}

	public class SetupBehaviorLeaf : LeafNode
	{

		readonly Info _Info;
		readonly float _Duration;

		public SetupBehaviorLeaf(Contexts contexts, AIEntity e
			, Info info
			, float duration) : base(contexts, e)
		{
			_Info = info;
			_Duration = duration;
		}

		public override BehaviorState Process()
		{
			SetupBehavior(_Entity);
			foreach (var id in _Entity.trackedFriends.ids)
			{
				var agent = _Contexts.aI.GetEntityWithId(id);
				SetupBehavior(agent);
			}
			return BehaviorState.Running;
		}

		void SetupBehavior(AIEntity e)
		{
			var direction = (Random.Range(0, 2) == 1) ? 1f : -1f;
			var playerMatcher = GameMatcher
				.AllOf(
					GameMatcher.Player
					, GameMatcher.View
					);
			var sequence = new SequenceNode(_Contexts, e
					, new List<BehaviorNode>() {
						new SetEntityToActingLeaf(_Contexts, e)
						, new SetPlayerAsTargetLeaf(_Contexts, e)
						, new ApplyAccelerationAmpLeaf(_Contexts, e, _Info.AccelerationAmp)
						, new TerminateOnDurationDecorator(_Contexts, e
							, new SimultaneousComposite(_Contexts, e
								, new List<BehaviorNode>() {
									new CircleTargetLeaf(_Contexts, e, direction)
									, new RepelFromLeaf(_Contexts, e
										, _Info.RepelRange
										, _Info.RepelAmp
										, playerMatcher
										)
									}
								)
							, _Duration
							, BehaviorState.Success
							)
						, new RemoveAccelerationAmpLeaf(_Contexts, e)
						}
					);
			sequence.Name = "Player Scrum";
			sequence.Begin();
			e.ReplaceBehavior(sequence);
		}
	}
}
