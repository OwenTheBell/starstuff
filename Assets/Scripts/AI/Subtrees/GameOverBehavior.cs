﻿using System.Collections.Generic;
using UnityEngine;

namespace GameOverBehavior
{
	[CreateAssetMenu(
		fileName = "Game Over Behavior"
		, menuName = "Starstuff/Behaviors/Game Over")]
	public class GameOverBehavior : SpecialBehavior
	{
		public Vector2 RepelRange;
		public float RepelAmp;

		public override BehaviorNode GenerateTree(Contexts contexts, AIEntity e)
		{
			var matcher = GameMatcher
				.AllOf(
					GameMatcher.Player
					, GameMatcher.View);
			var sequence = new SequenceNode(contexts, e
				, new List<BehaviorNode>() {
					new SetEntityToActingLeaf(contexts, e)
					, new SetPlayerAsTargetLeaf(contexts, e)
					, new TerminateOnDistanceToTargetDecorator(contexts, e
						, new RepelFromLeaf(contexts, e
							, RepelRange
							, RepelAmp
							, matcher
							)
						, RepelRange.y - 1
						, false
						, BehaviorState.Success
						)
					, new DestroySelfLeaf(contexts, e)
					}
				);
			sequence.Name = "Game Over Behavior";
			return sequence;
		}
	}
}
