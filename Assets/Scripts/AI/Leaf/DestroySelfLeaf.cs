﻿
public class DestroySelfLeaf : LeafNode 
{

	public DestroySelfLeaf(Contexts contexts, AIEntity e) : base(contexts, e) { }

	public override BehaviorState Process()
	{
		var agentsEntity = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		agentsEntity.isDestroying = true;
		_Entity.isDestroy = true;
		return BehaviorState.Success;
	}
}
