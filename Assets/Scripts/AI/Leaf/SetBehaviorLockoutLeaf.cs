﻿
class SetBehaviorLockoutLeaf : LeafNode
{

	private float _Adjust;

	public SetBehaviorLockoutLeaf(Contexts contexts, AIEntity e, float adjust = 1f) : base(contexts, e) {
		_Adjust = adjust;
	}

	public override BehaviorState Process()
	{
		var lockout = _Contexts.aI.behaviorLockoutTime;
		lockout.remaining = UnityEngine.Random.Range(lockout.min, lockout.max);
		lockout.remaining *= _Adjust;
		return BehaviorState.Success;
	}
}
