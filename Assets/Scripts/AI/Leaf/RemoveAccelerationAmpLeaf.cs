﻿
public class RemoveAccelerationAmpLeaf : LeafNode
{

	public RemoveAccelerationAmpLeaf(Contexts contexts, AIEntity e) : base(contexts, e) { }

	public override BehaviorState Process()
	{
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		if (agent.hasAccelerationAmp)
		{
			agent.RemoveAccelerationAmp();
		}
		return BehaviorState.Success;
	}
}
