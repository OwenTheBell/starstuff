﻿using Entitas;
using UnityEngine;

public class CircleTargetPositionLeaf : LeafNode
{

	readonly IGroup<GameEntity> _Players;
	readonly float _Direction;

	public CircleTargetPositionLeaf(
		Contexts contexts
		, AIEntity e
		, float direction) : base(contexts, e)
	{
		_Direction = direction;
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Player
				, GameMatcher.View
			);
		_Players = contexts.game.GetGroup(matcher);
	}

	public override BehaviorState Process()
	{
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		var myPosition = agent.view.value.Position;
		var targetPosition = _Entity.targetNearPlayer.position;
		var player = _Players.GetEntities()[0];
		targetPosition = player.view.value.Transform.TransformPoint(targetPosition);
		var radians = Mathf.Atan2(myPosition.y - targetPosition.y, myPosition.x - targetPosition.x);
		radians += _Direction * Mathf.PI / 2;
		var acceleration = new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
		var input = _Contexts.input.CreateEntity();
		input.AddTarget(agent.id.value);
		input.AddAccelerate(acceleration);
		return BehaviorState.Running;
	}
}