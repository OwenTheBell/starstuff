﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entitas;

public class RequireFriendCountLeaf : LeafNode
{

	private int _Count;
	private IGroup<GameEntity> _Friends;

	public RequireFriendCountLeaf(Contexts contexts, AIEntity e, int count) : base(contexts, e)
	{
		_Count = count;
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Friend
				, GameMatcher.View);
		_Friends = contexts.game.GetGroup(matcher);
	}

	public override BehaviorState Process()
	{
		if (_Friends.count >= _Count)
		{
			return BehaviorState.Success;
		}
		return BehaviorState.Failure;
	}
}
