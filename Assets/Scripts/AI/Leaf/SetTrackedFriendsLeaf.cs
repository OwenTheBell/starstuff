﻿using System.Collections.Generic;

public class SetTrackedFriendsLeaf : LeafNode
{

	protected readonly List<int> _Friends;

	public SetTrackedFriendsLeaf(
		Contexts contexts
		, AIEntity e
		, List<int> friends) : base(contexts, e)
	{
		_Friends = friends;
	}

	public override BehaviorState Process()
	{
		if (_Entity.hasTrackedFriends)
		{
			_Entity.RemoveTrackedFriends();
		}
		_Entity.AddTrackedFriends(_Friends);
		return BehaviorState.Success;
	}
}
