﻿using Entitas;
using UnityEngine;

public class MatchTargetVelocityLeaf : LeafNode
{

	readonly IGroup<GameEntity> _Entities;
	readonly Vector2 _Range;
	readonly int _Weight;

	public MatchTargetVelocityLeaf(
		Contexts contexts
		, AIEntity e
		, IMatcher<GameEntity> matcher
		, Vector2 range
		, int weight) : base(contexts, e)
	{
		_Entities = contexts.game.GetGroup(matcher);
		_Range = range;
		_Weight = weight;
	}

	public override BehaviorState Process()
	{
		if (_Entities.count == 0)
		{
			return BehaviorState.Running;
		}

		var velocity = Vector3.zero;
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		var position = agent.view.value.Position;
		foreach (var entity in _Entities.GetEntities())
		{
			var otherPosition = entity.view.value.Position;
			var distance = Vector3.Distance(position, otherPosition);
			var percent = Mathf.Clamp01((distance - _Range.x) / (_Range.y - _Range.x));
			velocity += entity.velocity.value * (1f - percent);
		}

		if (velocity.sqrMagnitude > 0.1f)
		{
			var acceleration = (velocity - agent.velocity.value).normalized;
			for (var i = 0; i < _Weight; i++)
			{
				var input = _Contexts.input.CreateEntity();
				input.AddTarget(agent.id.value);
				input.AddAccelerate(acceleration);
			}
		}
		return BehaviorState.Running;
	}
}