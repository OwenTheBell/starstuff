﻿using UnityEngine;

public class CircleOtherFriendsLeaf : LeafNode
{

	readonly float _Direction;

	public CircleOtherFriendsLeaf(
		Contexts contexts
		, AIEntity e
		, float direction) : base(contexts, e)
	{
		_Direction = direction;
	}

	public override BehaviorState Process()
	{
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		var myPosition = agent.view.value.Position;
		var position = agent.view.value.Position;
		foreach (var id in _Entity.trackedFriends.ids)
		{
			var friend = _Contexts.aI.GetEntityWithId(id);
			var otherAgent = _Contexts.game.GetEntityWithId(friend.agent.id);
			position += otherAgent.view.value.Position;
		}
		position /= _Entity.trackedFriends.ids.Count + 1;

		var radians = Mathf.Atan2(myPosition.y - position.y, myPosition.x - position.x);
		radians += _Direction * Mathf.PI / 2;

		var acceleration = new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
		var input = _Contexts.input.CreateEntity();
		input.AddTarget(agent.id.value);
		input.AddAccelerate(acceleration);

		return BehaviorState.Running;
	}
}
