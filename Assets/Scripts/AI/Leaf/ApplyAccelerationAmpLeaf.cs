﻿
public class ApplyAccelerationAmpLeaf : LeafNode
{

	readonly float _Amp;

	public ApplyAccelerationAmpLeaf(Contexts contexts, AIEntity e
		, float amp) : base(contexts, e)
	{
		_Amp = amp;
	}

	public override BehaviorState Process()
	{
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		if (agent.hasAccelerationAmp)
		{
			agent.ReplaceAccelerationAmp(_Amp);
		}
		else
		{
			agent.AddAccelerationAmp(_Amp);
		}
		return BehaviorState.Success;
	}
}
