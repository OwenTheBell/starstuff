﻿using UnityEngine;

public class RandomDelayLeaf : DelayLeaf
{
	readonly Vector2 _Range;

	public RandomDelayLeaf(
		Contexts contexts
		, AIEntity e
		, Vector2 range) : base(contexts, e, range.y)
	{
		_Range = range;
	}

	public override void Begin()
	{
		_RemainingDelay = Random.Range(_Range.x, _Range.y);
	}
}