﻿using Entitas;

public class GetCommonPointBetweenTargetFriendsLeaf : LeafNode
{

	readonly IGroup<GameEntity> _Players;

	public GetCommonPointBetweenTargetFriendsLeaf(Contexts contexts, AIEntity e) : base(contexts, e)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Player
				, GameMatcher.View
			);
		_Players = contexts.game.GetGroup(matcher);
	}

	public override BehaviorState Process()
	{
		if (!_Entity.hasTrackedFriends || _Players.count == 0)
		{
			return BehaviorState.Failure;
		}
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		var position = agent.view.value.Position;
		foreach (var id in _Entity.trackedFriends.ids)
		{
			var friend = _Contexts.aI.GetEntityWithId(id);
			position += _Contexts.game.GetEntityWithId(friend.agent.id).view.value.Position;
		}
		position /= _Entity.trackedFriends.ids.Count + 1;

		if (_Entity.hasTargetPosition)
		{
			_Entity.RemoveTargetPosition();
		}
		_Entity.AddTargetPosition(position);
		return BehaviorState.Success;
	}
}
