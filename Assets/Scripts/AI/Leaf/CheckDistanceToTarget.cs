﻿
public class CheckDistanceToTargetLeaf : LeafNode
{

	readonly float _Distance;

	public CheckDistanceToTargetLeaf(
		Contexts contexts
		, AIEntity e
		, float distance) : base(contexts, e)
	{
		_Distance = distance;
	}

	public override BehaviorState Process()
	{
		var target = _Contexts.game.GetEntityWithId(_Entity.target.id);

		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		var distance = UnityEngine.Vector2.Distance(target.view.value.Position
													, agent.view.value.Position);
		if (distance >= _Distance)
		{
			return BehaviorState.Success;
		}
		return BehaviorState.Failure;
	}
}
