﻿using UnityEngine;

public class SelectRandomTargetNearPlayerLeaf : LeafNode
{

	readonly float _Min;
	readonly float _Max;

	public SelectRandomTargetNearPlayerLeaf(
		Contexts contexts
		, AIEntity e
		, float min
		, float max) : base(contexts, e)
	{
		_Min = min;
		_Max = max;
	}

	public override BehaviorState Process()
	{
		var rad = Random.Range(0f, Mathf.PI * 2);
		var radius = Random.Range(_Min, _Max);
		var position = new Vector2(radius * Mathf.Cos(rad), radius * Mathf.Sin(rad));
		if (_Entity.hasTargetNearPlayer)
		{
			_Entity.RemoveTargetNearPlayer();
		}
		_Entity.AddTargetNearPlayer(position);
		return BehaviorState.Success;
	}

}