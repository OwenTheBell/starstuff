﻿using System.Linq;

public class SetRandomFriendAsTargetLeaf : LeafNode {

	public SetRandomFriendAsTargetLeaf(Contexts contexts, AIEntity e) : base(contexts, e) { }

	public override void Begin()
	{
		if (_Entity.hasTarget)
		{
			_Entity.RemoveTarget();
		}
	}

	public override BehaviorState Process()
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Friend
				, GameMatcher.View
				, GameMatcher.Visible);
		var friends = _Contexts.game.GetGroup(matcher).GetEntities().Where(e => e.aIAgent.id != _Entity.id.value).ToArray();
		if (friends.Length == 0)
		{
			return BehaviorState.Failure;
		}
		var index = UnityEngine.Random.Range(0, friends.Length);
		_Entity.AddTarget(friends[index].id.value);
		return BehaviorState.Success;
	}
}