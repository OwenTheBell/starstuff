﻿using Entitas;

public class CheckDistanceToLeaf : LeafNode
{

	readonly float _Distance;
	readonly IGroup<GameEntity> _Entities;

	public CheckDistanceToLeaf(
		Contexts contexts
		, AIEntity e
		, float distance
		, IMatcher<GameEntity> matcher) : base(contexts, e)
	{
		_Distance = distance;
		_Entities = contexts.game.GetGroup(matcher);
	}

	public override BehaviorState Process()
	{
		if (_Entities.count > 0)
		{
			foreach (var e in _Entities.GetEntities())
			{
				var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
				var distance = UnityEngine.Vector2.Distance(e.view.value.Position
															, agent.view.value.Position);
				if (distance >= _Distance)
				{
					return BehaviorState.Success;
				}
			}
		}
		return BehaviorState.Failure;
	}
}
