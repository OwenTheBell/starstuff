﻿using UnityEngine;
using System.Collections.Generic;

public class ChaseTargetLeaf : LeafNode
{

	float _Distance;
	RepelFromListLeaf _RepelLeaf;

	public ChaseTargetLeaf(Contexts contexts, AIEntity e, float distance) : base(contexts, e)
	{
		_Distance = distance;
	}

	public override void Begin()
	{
		base.Begin();
		_RepelLeaf = new RepelFromListLeaf(_Contexts, _Entity, new Vector2(_Distance, _Distance), 1f, new List<int>() { _Entity.target.id});
	}

	public override BehaviorState Process()
	{
		var target = _Contexts.game.GetEntityWithId(_Entity.target.id);
		var targetPosition = target.view.value.Position;
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		var myPosition = agent.view.value.Position;

		if (Vector2.Distance(myPosition, targetPosition) > _Distance)
		{
			var direction = targetPosition - myPosition;
			var input = _Contexts.input.CreateEntity();
			input.AddTarget(agent.id.value);
			input.AddAccelerate(direction);
		}
		else
		{
			_RepelLeaf.Process();
		}

		return BehaviorState.Running;
	}
}