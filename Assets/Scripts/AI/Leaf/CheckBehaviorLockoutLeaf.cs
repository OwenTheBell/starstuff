﻿
public class CheckBehaviorLockoutLeaf : LeafNode
{

	public CheckBehaviorLockoutLeaf(Contexts contexts, AIEntity e) : base(contexts, e) { }

	public override BehaviorState Process()
	{
		if (_Contexts.aI.behaviorLockoutTime.remaining <= 0f)
		{
			return BehaviorState.Success;
		}
		DebugUtils.Log("Lockout time not over yet");
		return BehaviorState.Failure;
	}
}