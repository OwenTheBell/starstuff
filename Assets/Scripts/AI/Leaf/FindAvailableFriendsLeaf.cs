﻿using System.Collections.Generic;
using Entitas;

public class FindAvailableFriendsLeaf : LeafNode
{

	readonly int _Min;
	readonly int _Max;
	IGroup<AIEntity> _Friends;

	public FindAvailableFriendsLeaf(Contexts contexts, AIEntity e, int min, int max) : base(contexts, e)
	{
		_Min = min;
		_Max = max;

		var matcher = AIMatcher
			.AllOf(
				AIMatcher.Agent
				, AIMatcher.Friend
				)
			.NoneOf(
				AIMatcher.Acting
				, AIMatcher.Cooldown
				);
		_Friends = _Contexts.aI.GetGroup(matcher);
	}

	public override void Begin()
	{
		base.Begin();
	}

	public override BehaviorState Process()
	{
		if (_Entity.hasTrackedFriends)
		{
			_Entity.RemoveTrackedFriends();
		}

		var availableFriends = new List<AIEntity>(_Friends.GetEntities());
		availableFriends.FilterAndRemove(e =>
		{
			var agentsEntity = _Contexts.game.GetEntityWithId(e.agent.id);
			return e.id.value == _Entity.id.value || !agentsEntity.isVisible;
		});

		if (availableFriends.Count < _Min)
		{
			return BehaviorState.Failure;
		}
		var max = (availableFriends.Count > _Max) ? _Max : availableFriends.Count;
		var count = UnityEngine.Random.Range(_Min, max + 1);

		var targettedFriends = new List<int>();
		while (count > 0)
		{
			var index = availableFriends.GetRandomIndex();
			targettedFriends.Add(availableFriends[index].id.value);
			availableFriends.RemoveAt(index);
			count--;
		}
		_Entity.AddTrackedFriends(targettedFriends);

		return BehaviorState.Success;
	}

}
