﻿
public class IAmVisibleLeaf : LeafNode
{

	public IAmVisibleLeaf(Contexts contexts, AIEntity e) : base(contexts, e) { }

	public override BehaviorState Process()
	{
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		return (agent.isVisible) ? BehaviorState.Success : BehaviorState.Failure;
	}
}
