﻿using UnityEngine;

public class RandomizedAccelerationLeaf : LeafNode
{

	readonly float _Duration;
	readonly float _Radians;
	readonly float _Amp;
	float _RemainingDuration;
	float _Direction;

	public RandomizedAccelerationLeaf(
		Contexts contexts
		, AIEntity e
		, float duration
		, float radians
		, float amp = 1f) : base(contexts, e)
	{
		_Duration = duration;
		_Radians = radians;
		_Amp = amp;
	}

	public override void Begin()
	{
		base.Begin();

		_RemainingDuration = _Duration;
		_Direction = Random.Range(-_Radians, _Radians);
	}

	public override BehaviorState Process()
	{
		_RemainingDuration -= _Contexts.game.time.Tick;
		if (_RemainingDuration > 0f)
		{
			var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
			var velocity = agent.velocity.value;
			var radians = Mathf.Atan2(velocity.y, velocity.x);
			radians += _Direction;
			var direction = new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
			var input = _Contexts.input.CreateEntity();
			input.AddTarget(_Entity.agent.id);
			input.AddAccelerate(direction * _Amp);
			return BehaviorState.Running;
		}
		return BehaviorState.Success;
	}
}
