﻿using UnityEngine;

public class CircleTargetLeaf : LeafNode 
{

	readonly float _Direction;

	public CircleTargetLeaf(
		Contexts contexts
		, AIEntity e
		, float direction) : base(contexts, e)
	{
		_Direction = direction;
	}

	public override BehaviorState Process()
	{
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		var target = _Contexts.game.GetEntityWithId(_Entity.target.id);
		if (target == null)
		{
			// return success because if this is part of a sequence we want it to 
			// continue operating
			return BehaviorState.Success;
		}
		var myPosition = agent.view.value.Position;
		var targetPosition = target.view.value.Position;
		var direction = (targetPosition - myPosition).normalized;
		var radians = Mathf.Atan2(myPosition.y - targetPosition.y, myPosition.x - targetPosition.x);
		radians += _Direction * Mathf.PI / 2;
		var acceleration = new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));

		var input = _Contexts.input.CreateEntity();
		input.AddTarget(agent.id.value);
		input.AddAccelerate(direction);

		return BehaviorState.Running;
	}
}
