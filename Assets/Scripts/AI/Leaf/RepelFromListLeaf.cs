﻿using System.Collections.Generic;
using UnityEngine;

public class RepelFromListLeaf : LeafNode
{
	readonly List<int> _Entities;
	readonly Vector2 _Range;
	readonly float _Amp;

	public RepelFromListLeaf(
		Contexts contexts
		, AIEntity e
		, Vector2 range
		, float amp
		, List<int> entities) : base(contexts, e)
	{
		_Range = range;
		_Entities = entities;
		_Amp = amp;
	}

	public override void Begin()
	{
		_Entities.RemoveAll(id => id == _Entity.agent.id);
	}

	public override BehaviorState Process()
	{
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		var position = agent.view.value.Position;
		var summedDirection = Vector3.zero;
		for (var i = _Entities.Count - 1; i >= 0; i--)
		{
			var id = _Entities[i];
			var target = _Contexts.game.GetEntityWithId(id);
			if (target == null)
			{
				_Entities.RemoveAt(i);
				continue;
			}
			var otherPosition = target.view.value.Position;
			var direction = otherPosition - position;
			direction = direction.normalized * -1f;
			var distance = Vector3.Distance(position, otherPosition);
			var percent = Mathf.Clamp01((distance - _Range.x) / (_Range.y - _Range.x));
			direction *= 1f - percent;
			summedDirection += direction;
		}
		if (_Entities.Count == 0)
		{
			return BehaviorState.Failure;
		}
		var input = _Contexts.input.CreateEntity();
		input.AddTarget(agent.id.value);
		input.AddAccelerate(summedDirection / _Entities.Count * _Amp);

		return BehaviorState.Running;
	}
}