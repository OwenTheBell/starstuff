﻿using Entitas;

public class SetPlayerAsTargetLeaf : LeafNode
{

	readonly IGroup<GameEntity> _Players;

	public SetPlayerAsTargetLeaf(Contexts contexts, AIEntity e) : base(contexts, e)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Player
				, GameMatcher.View);
		_Players = contexts.game.GetGroup(matcher);
	}

	public override BehaviorState Process()
	{
		if (_Players.count == 0)
		{
			return BehaviorState.Failure;
		}
		// only remove the target after confirming this behavior will run to ensure
		// that we can hold onto previous targets if desired
		if (_Entity.hasTarget)
		{
			_Entity.RemoveTarget();
		}
		var player = _Players.GetEntities()[0];
		_Entity.AddTarget(player.id.value);
		return BehaviorState.Success;
	}
}