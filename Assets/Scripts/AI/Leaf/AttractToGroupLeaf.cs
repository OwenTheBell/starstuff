﻿using UnityEngine;
using Entitas;

public class AttractToGroupLeaf : LeafNode
{

	readonly IGroup<GameEntity> _Entities;
	readonly Vector2 _Range;
	readonly float _Thrust;

	public AttractToGroupLeaf(
		Contexts contexts
		, AIEntity e
		, IMatcher<GameEntity> matcher
		, Vector2 range
		, float thrust) : base(contexts, e)
	{
		_Entities = contexts.game.GetGroup(matcher);
		_Range = range;
		_Thrust = thrust;
	}

	public override BehaviorState Process()
	{
		var center = Vector3.zero;
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		foreach (var entity in _Entities.GetEntities())
		{
			center += entity.view.value.Position;
		}
		center /= _Entities.count;
		var direction = center - agent.view.value.Position;
		var distance = Vector3.Distance(center, agent.view.value.Position);
		var percent = Mathf.Clamp01((distance - _Range.x) / (_Range.y - _Range.x));
		direction = direction.normalized * percent * _Thrust;

		var input = _Contexts.input.CreateEntity();
		input.AddTarget(agent.id.value);
		input.AddAccelerate(direction);

		return BehaviorState.Running;
	}
}
