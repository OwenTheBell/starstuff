﻿
public class ApplyAdjustMaximumVelocityLeaf : LeafNode
{

	float _Percent;

	public ApplyAdjustMaximumVelocityLeaf(Contexts contexts, AIEntity e, float percent) : base(contexts, e)
	{
		_Percent = percent;
	}

	public override BehaviorState Process()
	{
		var agentsEntity = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		if (agentsEntity.hasAdjustMaximumVelocity)
		{
			agentsEntity.RemoveAdjustMaximumVelocity();
		}
		agentsEntity.AddAdjustMaximumVelocity(_Percent);
		return BehaviorState.Success;
	}
}
