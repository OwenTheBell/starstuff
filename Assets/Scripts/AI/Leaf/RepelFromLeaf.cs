﻿using Entitas;
using UnityEngine;

public class RepelFromLeaf : LeafNode 
{

	readonly IGroup<GameEntity> _Entities;
	readonly Vector2 _Range;
	readonly float _Amp;

	public RepelFromLeaf(
		Contexts contexts
		, AIEntity e
		, Vector2 range
		, float amp
		, IMatcher<GameEntity> matcher) : base(contexts, e)
	{
		_Range = range;
		_Entities = contexts.game.GetGroup(matcher);
		_Amp = amp;
	}

	public override BehaviorState Process()
	{
		if (_Entities.count == 0)
		{
			return BehaviorState.Running;
		}
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		var position = agent.view.value.Position;
		var summedDirection = Vector3.zero;
		foreach (var target in _Entities.GetEntities())
		{
			var otherPosition = target.view.value.Position;
			var direction = otherPosition - position;
			direction = direction.normalized * -1f;
			var distance = Vector3.Distance(position, otherPosition);
			var percent = Mathf.Clamp01((distance - _Range.x) / (_Range.y - _Range.x));
			direction *= 1f - percent;
			summedDirection += direction;
		}
		var input = _Contexts.input.CreateEntity();
		input.AddTarget(agent.id.value);
		input.AddAccelerate(summedDirection / _Entities.count * _Amp);

		return BehaviorState.Running;	
	}
}