﻿
public class DestroyTargetLeaf : LeafNode
{
	public DestroyTargetLeaf(Contexts contexts, AIEntity e) : base(contexts, e) { }

	public override BehaviorState Process()
	{
		if (_Entity.hasTarget)
		{
			var target = _Contexts.game.GetEntityWithId(_Entity.target.id);
			if (target != null)
			{
				target.isDestroying = true;
			}
			_Entity.RemoveTarget();
		}
		return BehaviorState.Success;
	}
}
