﻿
public class DelayLeaf : LeafNode
{

	readonly float _Delay;
	protected float _RemainingDelay;

	public DelayLeaf(Contexts contexts, AIEntity e, float delay) : base(contexts, e)
	{
		_Delay = delay;
	}

	public override void Begin()
	{
		_RemainingDelay = _Delay;
	}

	public override BehaviorState Process()
	{
		var tick = _Contexts.game.time.Tick;
		_RemainingDelay -= tick;
		if (_RemainingDelay <= 0f)
		{
			return BehaviorState.Success;
		}
		return BehaviorState.Running;
	}
}
