﻿
public class SetEntityToActingLeaf : LeafNode
{

	public SetEntityToActingLeaf(Contexts contexts, AIEntity e) : base(contexts, e) { }

	public override BehaviorState Process()
	{
		_Entity.isActing = true;
		return BehaviorState.Success;
	}
}
