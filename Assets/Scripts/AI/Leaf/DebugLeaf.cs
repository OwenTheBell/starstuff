﻿
public class DebugLeaf : LeafNode
{

	readonly string _Message;

	public DebugLeaf(Contexts contexts, AIEntity e, string message) : base(contexts, e)
	{
		_Message = message;
	}

	public override BehaviorState Process()
	{
		UnityEngine.Debug.Log(_Message);
		return BehaviorState.Success;
	}
}
