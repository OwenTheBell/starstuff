﻿
public class RemoveAdjustMaximumVelocityLeaf : LeafNode 
{

	public RemoveAdjustMaximumVelocityLeaf(Contexts contexts, AIEntity e) : base(contexts, e) { }

	public override BehaviorState Process()
	{
		var agentsEntity = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		if (agentsEntity.hasAdjustMaximumVelocity)
		{
			agentsEntity.RemoveAdjustMaximumVelocity();
		}
		return BehaviorState.Success;
	}
}
