﻿using Entitas;

public class ChasePositionNearPlayerLeaf : LeafNode
{

	readonly IGroup<GameEntity> _Players;

	public ChasePositionNearPlayerLeaf(Contexts contexts, AIEntity e) : base(contexts, e)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Player
				, GameMatcher.View
			);
		_Players = contexts.game.GetGroup(matcher);
	}

	public override BehaviorState Process()
	{
		if (!_Entity.hasTargetNearPlayer || _Players.count == 0)
		{
			return BehaviorState.Failure;
		}
		var player = _Players.GetEntities()[0];
		var position = player.view.value.Transform.TransformPoint(_Entity.targetNearPlayer.position);

		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		var direction = position - agent.view.value.Position;
		var input = _Contexts.input.CreateEntity();
		input.AddTarget(agent.id.value);
		input.AddAccelerate(direction);
		return BehaviorState.Running;
	}
}
