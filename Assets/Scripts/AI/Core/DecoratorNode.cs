﻿
public abstract class DecoratorNode : BehaviorNode
{

	protected BehaviorNode _Child;

	public DecoratorNode(Contexts contexts, AIEntity e, BehaviorNode child) : base(contexts, e)
	{
		_Child = child;
	}

	public override void Begin()
	{
		_Child.Begin();
	}

	public override BehaviorState Process()
	{
		return _Child.Process();
	}

	public override void Teardown()
	{
		_Child.Teardown();
	}
}
