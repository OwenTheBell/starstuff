﻿
public abstract class BehaviorNode
{

	protected readonly AIEntity _Entity;
	protected readonly Contexts _Contexts;
	private string _Name = string.Empty;
	public string Name
	{
		get
		{
			return (_Name != string.Empty) ? _Name : GetType().ToString();
		}
		set
		{
			_Name = value;
		}
	}


	public BehaviorNode(Contexts contexts, AIEntity e)
	{
		_Contexts = contexts;
		_Entity = e;
	}

	public virtual void Begin() { }

	public abstract BehaviorState Process();

	public virtual void Teardown() { }
}

public enum BehaviorState
{
	Running,
	Success,
	Failure
}