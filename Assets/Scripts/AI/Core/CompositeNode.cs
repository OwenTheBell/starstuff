﻿using System.Collections.Generic;

public abstract class CompositeNode : BehaviorNode
{
	protected List<BehaviorNode> _Children;

	public CompositeNode(Contexts contexts, AIEntity e, List<BehaviorNode> children) : base(contexts, e)
	{
		_Children = children;
	}
}
