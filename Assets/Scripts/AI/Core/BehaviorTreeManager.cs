﻿using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using System.Linq;

public class BehaviorTreeManager : MonoBehaviour, IEntityConfiguration, IBehaviorManager
{

	[FoldoutGroup("Chase")]
	public Vector2 FriendRepelRange;
	[FoldoutGroup("Chase")]
	public float FriendRepelForce;
	[FoldoutGroup("Chase")]
	public Vector2 PlayerRepelRange;
	[FoldoutGroup("Chase")]
	public float PlayerRepelForce;

	[FoldoutGroup("Special behaviors")]
	public float BehaviorDelay;

	private Contexts _Contexts;
	private AIEntity _Entity;
	private BehaviorNode _RootNode;
	private BehaviorNode _OverrideNode;
	private List<BehaviorNode> _SpecialBehaviors;

	public void Configure(Contexts contexts, GameEntity e)
	{
		_Contexts = contexts;
		_Entity = contexts.aI.CreateEntity();
		e.AddAIAgent(_Entity.id.value);
		_Entity.AddAgent(e.id.value);
		_Entity.isFriend = true;
		_Entity.AddBehaviorManager(this);
		_SpecialBehaviors = new List<BehaviorNode>();

		var playerMatcher = GameMatcher
			.AllOf(
				GameMatcher.Player
				, GameMatcher.View
				);
		var friendMatcher = GameMatcher
			.AllOf(
				GameMatcher.Friend
				, GameMatcher.View
				);
		var nonactingFriendMatcher = GameMatcher
			.AllOf(
				GameMatcher.Friend
				, GameMatcher.View
			).NoneOf(
				GameMatcher.Acting
				);


		_RootNode = new SequenceNode(contexts, _Entity, new List<BehaviorNode>() {
			new TerminateOnDurationDecorator(contexts, _Entity
				, new SimultaneousComposite(contexts, _Entity, new List<BehaviorNode>() {
					new MatchTargetVelocityLeaf(_Contexts, _Entity
						, playerMatcher
						, new Vector2(10f, 20f)
						, 1
						)
					, new MatchTargetVelocityLeaf(_Contexts, _Entity
						, nonactingFriendMatcher
						, new Vector2(10f, 10f)
						, 1
						)
					, new RepelFromLeaf(contexts, _Entity
						, FriendRepelRange
						, FriendRepelForce
						, friendMatcher

						)
					, new RepelFromLeaf(contexts, _Entity
						, PlayerRepelRange
						, PlayerRepelForce
						, playerMatcher
						)
					, new AttractToGroupLeaf(contexts, _Entity
						, playerMatcher
						, new Vector2(5f, 10f)
						, 7f
						)
					// this provides some noise in the entity's movement
					, new LoopUntilDecorator(contexts, _Entity
						, new SequenceNode(contexts, _Entity, new List<BehaviorNode>() {
							new RandomDelayLeaf(contexts, _Entity, new Vector2(1f, 2f))
							, new RandomizedAccelerationLeaf(contexts, _Entity
								, 2f
								, 1.6f
								, 3f
								)
							})
						, BehaviorState.Failure
						)
					})
				, BehaviorDelay
				, BehaviorState.Success
				)
			// special behaviors
			, new SequenceNode(contexts, _Entity
				, new List<BehaviorNode>() {
					new IAmVisibleLeaf(contexts, _Entity)
					, new CheckBehaviorLockoutLeaf(contexts, _Entity)
					, new RandomizedSelectorComposite(contexts, _Entity, _SpecialBehaviors)
					}
				)
			});

		_Entity.AddBehavior(_RootNode);
		_Entity.behavior.value.Begin();
		enabled = true;
	}

	void Update()
	{
		var state = _Entity.behavior.value.Process();
		if (state == BehaviorState.Success || state == BehaviorState.Failure)
		{
			_Entity.isActing = false;
			_RootNode.Begin();
			_Entity.ReplaceBehavior(_RootNode);
		}
	}

	void IBehaviorManager.AddBehaviors(List<BehaviorNode> nodes)
	{
		_SpecialBehaviors.Clear();
		_SpecialBehaviors.AddRange(nodes);
	}
}