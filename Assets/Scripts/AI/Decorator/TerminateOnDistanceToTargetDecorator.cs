﻿using UnityEngine;

public class TerminateOnDistanceToTargetDecorator : DecoratorNode
{

	readonly float _Distance;
	readonly bool _LessThan;
	readonly BehaviorState _ReturnValue;

	int _PlayerID;

	public TerminateOnDistanceToTargetDecorator(
		Contexts contexts
		, AIEntity e
		, BehaviorNode child
		, float distance
		, bool lessThan
		, BehaviorState returnValue) : base(contexts, e, child)
	{
		_Distance = distance;
		_LessThan = lessThan;
		_ReturnValue = returnValue;
	}

	public override BehaviorState Process()
	{
		var target = _Contexts.game.GetEntityWithId(_Entity.target.id);
		var state = base.Process();
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		// If the distance criteria has been met, return _ReturnValue to  end processing this
		var distance = Vector2.Distance(target.view.value.Position, agent.view.value.Position);
		if ((distance < _Distance && _LessThan)
			|| (distance > _Distance && !_LessThan))
		{
			return _ReturnValue;
		}
		return state;
	}
}
