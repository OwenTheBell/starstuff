﻿
public class TerminateOnDurationDecorator : DecoratorNode
{

	readonly float _Duration;
	readonly BehaviorState _ReturnValue;

	private float _Remaining;

	public TerminateOnDurationDecorator(
		Contexts contexts
		, AIEntity e
		, BehaviorNode child
		, float duration
		, BehaviorState returnValue) : base(contexts, e, child)
	{
		_Duration = duration;
		_ReturnValue = returnValue;
	}

	public override void Begin()
	{
		base.Begin();
		_Remaining = _Duration;
	}

	public override BehaviorState Process()
	{
		var baseState = base.Process();

		_Remaining -= _Contexts.game.time.Tick;
		if (_Remaining <= 0f)
		{
			return _ReturnValue;
		}
		return baseState;
	}
}