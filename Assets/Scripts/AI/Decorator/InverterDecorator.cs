﻿
public class InverterDecorator : DecoratorNode
{

	public InverterDecorator(
		Contexts contexts
		, AIEntity e
		, BehaviorNode child) : base(contexts, e, child) { }

	public override BehaviorState Process()
	{
		var state = base.Process();
		if (state == BehaviorState.Success)
		{
			return BehaviorState.Failure;
		}
		else if (state == BehaviorState.Failure)
		{
			return BehaviorState.Success;
		}
		return BehaviorState.Running;
	}
}
