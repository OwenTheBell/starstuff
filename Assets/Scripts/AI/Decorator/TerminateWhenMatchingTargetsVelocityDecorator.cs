﻿
public class TerminateWhenRelativeToTargetsVelocityDecorator : DecoratorNode
{

	readonly float _Adjustor;
	readonly bool _LessThan;

	public TerminateWhenRelativeToTargetsVelocityDecorator(
		Contexts contexts, 
		AIEntity e,
		BehaviorNode child,
		float adjustor,
		bool lessThan) : base(contexts, e, child)
	{
		_Adjustor = adjustor;
		_LessThan = lessThan;
	}

	public override BehaviorState Process()
	{
		var target = _Contexts.game.GetEntityWithId(_Entity.target.id);
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);
		if (target == null || !target.hasVelocity || !agent.hasVelocity)
		{
			return BehaviorState.Failure;
		}

		var tVelocity = target.velocity.value.sqrMagnitude * _Adjustor;
		var aVelocity = agent.velocity.value.sqrMagnitude;
		if ((_LessThan && tVelocity > aVelocity)
			|| (!_LessThan && tVelocity < aVelocity))
		{
			return BehaviorState.Success;
		}
		return base.Process();
	}
}