﻿
public class LoopUntilDecorator : DecoratorNode 
{

	readonly BehaviorState _UntilState;

	public LoopUntilDecorator (
		Contexts contexts
		, AIEntity e
		, BehaviorNode child
		, BehaviorState state) :  base(contexts, e, child)
	{
		_UntilState = state;
	}

	public override BehaviorState Process()
	{
		var state = base.Process();
		if (state == BehaviorState.Running)
		{
			return state;
		}
		else if (state != _UntilState)
		{
			_Child.Begin();
			return BehaviorState.Running;
		}
		return BehaviorState.Success;
	}
}