﻿using Entitas;
using UnityEngine;

public class TerminateOnDistanceToTargetPositionNearPlayerDecorator : DecoratorNode
{

	readonly IGroup<GameEntity> _Players;
	readonly float _Distance;

	public TerminateOnDistanceToTargetPositionNearPlayerDecorator(
		Contexts contexts
		, AIEntity e
		, BehaviorNode child
		, float distance) : base(contexts, e, child)
	{
		_Distance = distance;
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Player,
				GameMatcher.View);
		_Players = contexts.game.GetGroup(matcher);
	}

	public override BehaviorState Process()
	{
		var state = base.Process();
		if (state == BehaviorState.Failure)
		{
			return state;
		}
		var player = _Players.GetEntities()[0];
		var agent = _Contexts.game.GetEntityWithId(_Entity.agent.id);

		var position = _Entity.targetNearPlayer.position;
		position = player.view.value.Transform.TransformPoint(position);
		var distance = Vector3.Distance(position, agent.view.value.Position);
		if (distance < _Distance)
		{
			return BehaviorState.Success;
		}
		return state;
	}
}
