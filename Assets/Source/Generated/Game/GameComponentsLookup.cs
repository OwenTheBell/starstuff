//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentLookupGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public static class GameComponentsLookup {

    public const int AccelerateCommandsThisFrame = 0;
    public const int Accelerate = 1;
    public const int AccelerationAmp = 2;
    public const int Acceleration = 3;
    public const int Acting = 4;
    public const int Active = 5;
    public const int ActiveListener = 6;
    public const int ActiveRemovedListener = 7;
    public const int AdjustMaximumVelocity = 8;
    public const int AIAgent = 9;
    public const int AngularAcceleration = 10;
    public const int AngularVelocity = 11;
    public const int AngularVelocityDamping = 12;
    public const int Destroying = 13;
    public const int DestroyingListener = 14;
    public const int Facing = 15;
    public const int FacingListener = 16;
    public const int Following = 17;
    public const int FollowingListener = 18;
    public const int Friend = 19;
    public const int FromPrototype = 20;
    public const int GameAccelerateListener = 21;
    public const int GameAccelerateRemovedListener = 22;
    public const int GameTurnListener = 23;
    public const int GameTurnRemovedListener = 24;
    public const int Id = 25;
    public const int LockoutControl = 26;
    public const int MaximumAngularVelocity = 27;
    public const int MaximumVelocity = 28;
    public const int MovementAdjust = 29;
    public const int Paused = 30;
    public const int Player = 31;
    public const int Prototype = 32;
    public const int ScalingAcceleration = 33;
    public const int ScalingMaximumVelocity = 34;
    public const int SpawnInterval = 35;
    public const int Time = 36;
    public const int TimeListener = 37;
    public const int Turn = 38;
    public const int Velocity = 39;
    public const int VelocityDamping = 40;
    public const int VelocityListener = 41;
    public const int View = 42;
    public const int Visible = 43;
    public const int VisibleListener = 44;

    public const int TotalComponents = 45;

    public static readonly string[] componentNames = {
        "AccelerateCommandsThisFrame",
        "Accelerate",
        "AccelerationAmp",
        "Acceleration",
        "Acting",
        "Active",
        "ActiveListener",
        "ActiveRemovedListener",
        "AdjustMaximumVelocity",
        "AIAgent",
        "AngularAcceleration",
        "AngularVelocity",
        "AngularVelocityDamping",
        "Destroying",
        "DestroyingListener",
        "Facing",
        "FacingListener",
        "Following",
        "FollowingListener",
        "Friend",
        "FromPrototype",
        "GameAccelerateListener",
        "GameAccelerateRemovedListener",
        "GameTurnListener",
        "GameTurnRemovedListener",
        "Id",
        "LockoutControl",
        "MaximumAngularVelocity",
        "MaximumVelocity",
        "MovementAdjust",
        "Paused",
        "Player",
        "Prototype",
        "ScalingAcceleration",
        "ScalingMaximumVelocity",
        "SpawnInterval",
        "Time",
        "TimeListener",
        "Turn",
        "Velocity",
        "VelocityDamping",
        "VelocityListener",
        "View",
        "Visible",
        "VisibleListener"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(AccelerateCommandsThisFrameComponent),
        typeof(AccelerateComponent),
        typeof(AccelerationAmpComponent),
        typeof(AccelerationComponent),
        typeof(ActingComponent),
        typeof(ActiveComponent),
        typeof(ActiveListenerComponent),
        typeof(ActiveRemovedListenerComponent),
        typeof(AdjustMaximumVelocityComponent),
        typeof(AIAgentComponent),
        typeof(AngularAccelerationComponent),
        typeof(AngularVelocityComponent),
        typeof(AngularVelocityDampingComponent),
        typeof(DestroyingComponent),
        typeof(DestroyingListenerComponent),
        typeof(FacingComponent),
        typeof(FacingListenerComponent),
        typeof(FollowingComponent),
        typeof(FollowingListenerComponent),
        typeof(FriendComponent),
        typeof(FromPrototypeComponent),
        typeof(GameAccelerateListenerComponent),
        typeof(GameAccelerateRemovedListenerComponent),
        typeof(GameTurnListenerComponent),
        typeof(GameTurnRemovedListenerComponent),
        typeof(IdComponent),
        typeof(LockoutControlComponent),
        typeof(MaximumAngularVelocityComponent),
        typeof(MaximumVelocityComponent),
        typeof(MovementAdjustComponent),
        typeof(PausedComponent),
        typeof(PlayerComponent),
        typeof(PrototypeComponent),
        typeof(ScalingAccelerationComponent),
        typeof(ScalingMaximumVelocityComponent),
        typeof(SpawnIntervalComponent),
        typeof(TimeComponent),
        typeof(TimeListenerComponent),
        typeof(TurnComponent),
        typeof(VelocityComponent),
        typeof(VelocityDampingComponent),
        typeof(VelocityListenerComponent),
        typeof(ViewComponent),
        typeof(VisibleComponent),
        typeof(VisibleListenerComponent)
    };
}
