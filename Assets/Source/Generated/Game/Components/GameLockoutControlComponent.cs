//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    static readonly LockoutControlComponent lockoutControlComponent = new LockoutControlComponent();

    public bool isLockoutControl {
        get { return HasComponent(GameComponentsLookup.LockoutControl); }
        set {
            if (value != isLockoutControl) {
                var index = GameComponentsLookup.LockoutControl;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : lockoutControlComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherLockoutControl;

    public static Entitas.IMatcher<GameEntity> LockoutControl {
        get {
            if (_matcherLockoutControl == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.LockoutControl);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherLockoutControl = matcher;
            }

            return _matcherLockoutControl;
        }
    }
}
