//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public GameAccelerateListenerComponent gameAccelerateListener { get { return (GameAccelerateListenerComponent)GetComponent(GameComponentsLookup.GameAccelerateListener); } }
    public bool hasGameAccelerateListener { get { return HasComponent(GameComponentsLookup.GameAccelerateListener); } }

    public void AddGameAccelerateListener(System.Collections.Generic.List<IGameAccelerateListener> newValue) {
        var index = GameComponentsLookup.GameAccelerateListener;
        var component = CreateComponent<GameAccelerateListenerComponent>(index);
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceGameAccelerateListener(System.Collections.Generic.List<IGameAccelerateListener> newValue) {
        var index = GameComponentsLookup.GameAccelerateListener;
        var component = CreateComponent<GameAccelerateListenerComponent>(index);
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveGameAccelerateListener() {
        RemoveComponent(GameComponentsLookup.GameAccelerateListener);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherGameAccelerateListener;

    public static Entitas.IMatcher<GameEntity> GameAccelerateListener {
        get {
            if (_matcherGameAccelerateListener == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.GameAccelerateListener);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherGameAccelerateListener = matcher;
            }

            return _matcherGameAccelerateListener;
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public void AddGameAccelerateListener(IGameAccelerateListener value) {
        var listeners = hasGameAccelerateListener
            ? gameAccelerateListener.value
            : new System.Collections.Generic.List<IGameAccelerateListener>();
        listeners.Add(value);
        ReplaceGameAccelerateListener(listeners);
    }

    public void RemoveGameAccelerateListener(IGameAccelerateListener value, bool removeComponentWhenEmpty = true) {
        var listeners = gameAccelerateListener.value;
        listeners.Remove(value);
        if (removeComponentWhenEmpty && listeners.Count == 0) {
            RemoveGameAccelerateListener();
        } else {
            ReplaceGameAccelerateListener(listeners);
        }
    }
}
