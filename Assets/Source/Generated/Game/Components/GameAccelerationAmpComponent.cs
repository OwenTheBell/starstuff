//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public AccelerationAmpComponent accelerationAmp { get { return (AccelerationAmpComponent)GetComponent(GameComponentsLookup.AccelerationAmp); } }
    public bool hasAccelerationAmp { get { return HasComponent(GameComponentsLookup.AccelerationAmp); } }

    public void AddAccelerationAmp(float newValue) {
        var index = GameComponentsLookup.AccelerationAmp;
        var component = CreateComponent<AccelerationAmpComponent>(index);
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceAccelerationAmp(float newValue) {
        var index = GameComponentsLookup.AccelerationAmp;
        var component = CreateComponent<AccelerationAmpComponent>(index);
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveAccelerationAmp() {
        RemoveComponent(GameComponentsLookup.AccelerationAmp);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherAccelerationAmp;

    public static Entitas.IMatcher<GameEntity> AccelerationAmp {
        get {
            if (_matcherAccelerationAmp == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.AccelerationAmp);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherAccelerationAmp = matcher;
            }

            return _matcherAccelerationAmp;
        }
    }
}
