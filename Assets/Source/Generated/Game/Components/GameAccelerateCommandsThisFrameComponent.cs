//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public AccelerateCommandsThisFrameComponent accelerateCommandsThisFrame { get { return (AccelerateCommandsThisFrameComponent)GetComponent(GameComponentsLookup.AccelerateCommandsThisFrame); } }
    public bool hasAccelerateCommandsThisFrame { get { return HasComponent(GameComponentsLookup.AccelerateCommandsThisFrame); } }

    public void AddAccelerateCommandsThisFrame(int newValue) {
        var index = GameComponentsLookup.AccelerateCommandsThisFrame;
        var component = CreateComponent<AccelerateCommandsThisFrameComponent>(index);
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceAccelerateCommandsThisFrame(int newValue) {
        var index = GameComponentsLookup.AccelerateCommandsThisFrame;
        var component = CreateComponent<AccelerateCommandsThisFrameComponent>(index);
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveAccelerateCommandsThisFrame() {
        RemoveComponent(GameComponentsLookup.AccelerateCommandsThisFrame);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherAccelerateCommandsThisFrame;

    public static Entitas.IMatcher<GameEntity> AccelerateCommandsThisFrame {
        get {
            if (_matcherAccelerateCommandsThisFrame == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.AccelerateCommandsThisFrame);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherAccelerateCommandsThisFrame = matcher;
            }

            return _matcherAccelerateCommandsThisFrame;
        }
    }
}
