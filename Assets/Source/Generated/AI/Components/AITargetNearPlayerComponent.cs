//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class AIEntity {

    public TargetNearPlayerComponent targetNearPlayer { get { return (TargetNearPlayerComponent)GetComponent(AIComponentsLookup.TargetNearPlayer); } }
    public bool hasTargetNearPlayer { get { return HasComponent(AIComponentsLookup.TargetNearPlayer); } }

    public void AddTargetNearPlayer(UnityEngine.Vector3 newPosition) {
        var index = AIComponentsLookup.TargetNearPlayer;
        var component = CreateComponent<TargetNearPlayerComponent>(index);
        component.position = newPosition;
        AddComponent(index, component);
    }

    public void ReplaceTargetNearPlayer(UnityEngine.Vector3 newPosition) {
        var index = AIComponentsLookup.TargetNearPlayer;
        var component = CreateComponent<TargetNearPlayerComponent>(index);
        component.position = newPosition;
        ReplaceComponent(index, component);
    }

    public void RemoveTargetNearPlayer() {
        RemoveComponent(AIComponentsLookup.TargetNearPlayer);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class AIMatcher {

    static Entitas.IMatcher<AIEntity> _matcherTargetNearPlayer;

    public static Entitas.IMatcher<AIEntity> TargetNearPlayer {
        get {
            if (_matcherTargetNearPlayer == null) {
                var matcher = (Entitas.Matcher<AIEntity>)Entitas.Matcher<AIEntity>.AllOf(AIComponentsLookup.TargetNearPlayer);
                matcher.componentNames = AIComponentsLookup.componentNames;
                _matcherTargetNearPlayer = matcher;
            }

            return _matcherTargetNearPlayer;
        }
    }
}
