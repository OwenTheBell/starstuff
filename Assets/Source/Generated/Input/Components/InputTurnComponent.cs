//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class InputEntity {

    public TurnComponent turn { get { return (TurnComponent)GetComponent(InputComponentsLookup.Turn); } }
    public bool hasTurn { get { return HasComponent(InputComponentsLookup.Turn); } }

    public void AddTurn(float newDirection) {
        var index = InputComponentsLookup.Turn;
        var component = CreateComponent<TurnComponent>(index);
        component.direction = newDirection;
        AddComponent(index, component);
    }

    public void ReplaceTurn(float newDirection) {
        var index = InputComponentsLookup.Turn;
        var component = CreateComponent<TurnComponent>(index);
        component.direction = newDirection;
        ReplaceComponent(index, component);
    }

    public void RemoveTurn() {
        RemoveComponent(InputComponentsLookup.Turn);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiInterfaceGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class InputEntity : ITurnEntity { }

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class InputMatcher {

    static Entitas.IMatcher<InputEntity> _matcherTurn;

    public static Entitas.IMatcher<InputEntity> Turn {
        get {
            if (_matcherTurn == null) {
                var matcher = (Entitas.Matcher<InputEntity>)Entitas.Matcher<InputEntity>.AllOf(InputComponentsLookup.Turn);
                matcher.componentNames = InputComponentsLookup.componentNames;
                _matcherTurn = matcher;
            }

            return _matcherTurn;
        }
    }
}
