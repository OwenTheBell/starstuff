//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CommandEntity {

    public AccelerateComponent accelerate { get { return (AccelerateComponent)GetComponent(CommandComponentsLookup.Accelerate); } }
    public bool hasAccelerate { get { return HasComponent(CommandComponentsLookup.Accelerate); } }

    public void AddAccelerate(UnityEngine.Vector3 newValue) {
        var index = CommandComponentsLookup.Accelerate;
        var component = CreateComponent<AccelerateComponent>(index);
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceAccelerate(UnityEngine.Vector3 newValue) {
        var index = CommandComponentsLookup.Accelerate;
        var component = CreateComponent<AccelerateComponent>(index);
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveAccelerate() {
        RemoveComponent(CommandComponentsLookup.Accelerate);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiInterfaceGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CommandEntity : IAccelerateEntity { }

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CommandMatcher {

    static Entitas.IMatcher<CommandEntity> _matcherAccelerate;

    public static Entitas.IMatcher<CommandEntity> Accelerate {
        get {
            if (_matcherAccelerate == null) {
                var matcher = (Entitas.Matcher<CommandEntity>)Entitas.Matcher<CommandEntity>.AllOf(CommandComponentsLookup.Accelerate);
                matcher.componentNames = CommandComponentsLookup.componentNames;
                _matcherAccelerate = matcher;
            }

            return _matcherAccelerate;
        }
    }
}
