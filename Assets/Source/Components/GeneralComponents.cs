﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Event(EventTarget.Self, EventType.Added), Event(EventTarget.Self, EventType.Removed)]
public class ActiveComponent : IComponent { }

[Game]
public class FromPrototypeComponent : IComponent
{
	public int id;
}

[Input, Command, Game, AI]
public class IdComponent : IComponent
{
	[PrimaryEntityIndex]
	public int value;
}

[Game, Unique]
public class PausedComponent : IComponent { }

[Game]
public class PrototypeComponent : IComponent
{
	public int poolIndex;
}

[Input, Command, AI]
public class TargetComponent : IComponent
{
	public int id;
}

[Game, Unique, Event(EventTarget.Any, EventType.Added)]
public class TimeComponent : IComponent
{
	public float Tick;
	public float Time;
	public float Scale;
	public float FixedTick;
	public float UnpausedTick;
}

[Game]
public class ViewComponent : IComponent
{
	public IView value;
}

[Game, Event(EventTarget.Self, EventType.Added)]
public class DestroyingComponent : IComponent { }

[Game, Event(EventTarget.Self, EventType.Added)]
public class VisibleComponent : IComponent { }

[Game]
public class LockoutControlComponent : IComponent { }