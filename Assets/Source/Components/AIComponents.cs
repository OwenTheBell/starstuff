﻿using System.Collections.Generic;
using Entitas;
using Entitas.CodeGeneration.Attributes;

/// <summary>
/// Identifies if an Agent is in the middle of performing an action
/// </summary>
[AI, Game]
public class ActingComponent : IComponent { }

[AI, Unique]
public class ActingCooldownComponent : IComponent
{
	public float duration;
}

[AI]
public class CooldownComponent : IComponent
{
	public float remaining;
}

[AI]
public class AgentComponent : IComponent
{
	public int id;
}

[AI]
public class TrackedFriendsComponent : IComponent
{
	public List<int> ids;
}

[AI]
public class BehaviorComponent : IComponent
{
	public BehaviorNode value;
}

[AI]
public class TargetNearPlayerComponent : IComponent
{
	public UnityEngine.Vector3 position;
}

[AI]
public class TargetPositionComponent : IComponent
{
	public UnityEngine.Vector3 value;
}

[AI]
public class DestroyComponent : IComponent { }

[AI, Unique]
public class BehaviorLockoutTimeComponent : IComponent
{
	public float min;
	public float max;
	public float remaining;
}

[AI]
public class BehaviorManagerComponent : IComponent
{
	public IBehaviorManager value;
}