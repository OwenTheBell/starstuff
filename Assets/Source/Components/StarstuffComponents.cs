﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Input, Command, Game, Event(EventTarget.Self), Event(EventTarget.Self, EventType.Removed)]
public class AccelerateComponent : IComponent
{
	public UnityEngine.Vector3 value;
}

[Game]
public class AccelerateCommandsThisFrameComponent : IComponent
{
	public int value;
}

[Game]
public class AccelerationComponent : IComponent
{
	public float value;
}

[Game]
public class AccelerationAmpComponent : IComponent
{
	public float value;
}

[Game]
public class AIAgentComponent : IComponent
{
	public int id;
}

[Game]
public class AngularAccelerationComponent : IComponent
{
	public float value;
}

[Game]
public class AngularVelocityComponent : IComponent
{
	public float value;
}

[Game]
public class AngularVelocityDampingComponent : IComponent
{
	public float value;
}

[Game, Event(EventTarget.Self, EventType.Added)]
public class FacingComponent : IComponent
{
	public float value;
}

[Game, Event(EventTarget.Any, EventType.Added)]
public class FollowingComponent : IComponent
{

}

[Game, AI]
public class FriendComponent : IComponent
{

}

[Game]
public class MaximumAngularVelocityComponent : IComponent
{
	public float value;
}

[Game]
public class MaximumVelocityComponent : IComponent
{
	public float value;
}

[Game]
public class MovementAdjustComponent : IComponent
{
	public float Acceleration;
	public float MaxVelocity;
}

[Game]
public class PlayerComponent : IComponent { }

[Game]
public class ScalingAccelerationComponent : IComponent
{
	public float percent;
}

[Game]
public class ScalingMaximumVelocityComponent : IComponent
{
	public float percent;
}

[Game]
public class AdjustMaximumVelocityComponent : IComponent
{
	public float percent;
}

[Game]
public class SpawnIntervalComponent : IComponent
{
	public float split;
	public float duration;
}

[Input]
public class ThrustComponent : IComponent
{

}

[Input, Event(EventTarget.Any, EventType.Added), Event(EventTarget.Any, EventType.Removed), Command, Game]
public class TurnComponent : IComponent
{
	public float direction;
}

[Game, Event(EventTarget.Self, EventType.Added)]
public class VelocityComponent : IComponent
{
	public UnityEngine.Vector3 value;
}

[Game]
public class VelocityDampingComponent : IComponent
{
	public float value;
}