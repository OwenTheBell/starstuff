﻿using UnityEngine;

public interface IPrototypeService {
    GameObject Create(GameEntity entity, GameObject prototypeGO);
    GameObject Create(GameEntity entity, GameObject prototypeGO, string name);
    void ReplaceGO(GameEntity entity, GameObject prototypeGO);
    void ChangeName(GameEntity prototype, string name);
    GameObject GetPrototypeGO(GameEntity prototype);
    GameEntity CreateFromPrototype(GameEntity prototype);
}