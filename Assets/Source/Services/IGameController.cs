﻿
public interface IGameController {
    Contexts AllContexts { get; }
    SystemFeature Systems { get; }

    void Begin();
    void TearDown();
    void Reset();
}