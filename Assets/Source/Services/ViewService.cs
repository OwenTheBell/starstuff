﻿using UnityEngine;

/// <summary>
/// Handles connecting Entities and GameObjects via a ViewController
/// </summary>
public class ViewService : IViewService {

    readonly Contexts _Contexts;

    public ViewService(Contexts contexts) {
        _Contexts = contexts;
    }

    public void Create(Entitas.IEntity e, GameObject gO) {
        ViewController controller;
        if (gO.HasComponent(typeof(ViewController))) {
            controller = gO.GetComponent<ViewController>();
        }
        else {
            controller = gO.AddComponent<ViewController>();
        }
        controller.Initialize(_Contexts, e);
    }
}