﻿using UnityEngine.Assertions;

public partial class Services {

    public static Services Instance { get; private set; }

    public IViewService View;
    public IGameController GameController;
    public IPrototypeService Prototype;
    public ISoundService Sound;

    public Services(Contexts contexts) {
        Assert.IsNull(Instance, "You cannot reset the instance of Services");
        Instance = this;
        View = new ViewService(contexts);
        //Prototype = new PrototypeService(contexts);
    }
}