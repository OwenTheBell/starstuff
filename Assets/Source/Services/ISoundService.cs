﻿using System;
using UnityEngine;

public interface ISoundService {
    void Play(string audioEvent, float delay = 0f, GameObject source = null);
    void AddObserverToPrototype(int prototypeIndex, Type ObserverType, string audioEvent, float delay = 0f, bool overrideExisting = false);
}
