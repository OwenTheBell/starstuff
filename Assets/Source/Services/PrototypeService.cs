﻿using UnityEngine;
using Common.ObjectPooling;
using System.Collections.Generic;
using UnityEngine.Assertions;
using System.Text.RegularExpressions;

/// <summary>
/// Handles:
///     - creating prototypes
///     - connecting them to the object pool
///     - spawning entities based on them.
/// </summary>
public class PrototypeService : IPrototypeService
{

	readonly Contexts _contexts;
	Vector3 _prototypePosition;

	public PrototypeService(Contexts contexts)
	{
		_contexts = contexts;
		_prototypePosition = new Vector3(0f, 0f, 0f);
	}

	#region Interface Methods

	/// <summary>
	/// Turn an entity into a prototype associated with a gameobject
	/// </summary>
	/// <param name="prototype">Entity to become a prototype</param>
	/// <param name="prototypeGO">Initialized gameobject to be associated with prototype</param>
	/// <returns></returns>
	public GameObject Create(GameEntity prototype, GameObject prototypeGO)
	{
		var name = prototypeGO.name.Replace("(Clone)", "");
		return Create(prototype, prototypeGO, name);
	}

	/// <summary>
	/// Turn an entity into a prototype associated with a gameobject
	/// </summary>
	/// <param name="prototype">Entity to become a prototype</param>
	/// <param name="prototypeGO">Initialized gameobject to be assoicated with prototype</param>
	/// <param name="name">Name for the prototype</param>
	/// <returns></returns>
	public GameObject Create(GameEntity prototype, GameObject prototypeGO, string name)
	{
		name += " ID: " + prototype.id.value;

		var children = prototypeGO.ListOfChildrenWithComponent<Renderer>();
		var index = ObjectPoolManager.Instance.AddToObjectPool(prototypeGO, name);
		prototype.AddPrototype(index);
		return prototypeGO;
	}

	/// <summary>
	/// Replace the gameobject associated with the prototype
	/// </summary>
	/// <param name="prototype">Prototype to change</param>
	/// <param name="gO">New gameobject to associate with prototype</param>
	public void ReplaceGO(GameEntity prototype, GameObject gO)
	{
		var oldGO = GetPrototypeGO(prototype);
		if (oldGO != gO)
		{
			GameObject.Destroy(oldGO);
		}
		ObjectPoolManager.Instance.ReplaceBlueprint(prototype.prototype.poolIndex, gO);
		var children = gO.ListOfChildrenWithComponent<Renderer>();
		var name = ObjectPoolManager.Instance.GetName(prototype.prototype.poolIndex);
		name = Regex.Replace(name, "ID:.*", "ID: " + prototype.id.value);
		ObjectPoolManager.Instance.ChangeName(prototype.prototype.poolIndex, name);
	}

	/// <summary>
	/// Change the name of a prototype
	/// </summary>
	/// <param name="prototype">Prototype to change</param>
	/// <param name="name">New name</param>
	public void ChangeName(GameEntity prototype, string name)
	{
		ObjectPoolManager.Instance.ChangeName(prototype.prototype.poolIndex, name + " ID: " + prototype.id.value);
	}

	/// <summary>
	/// Get the gameobject associate with the prototype
	/// </summary>
	/// <param name="prototype"></param>
	/// <returns></returns>
	public GameObject GetPrototypeGO(GameEntity prototype)
	{
		Assert.IsTrue(prototype.hasPrototype, "Attempting to get a prototypeGO for a non-prototype entity");
		return ObjectPoolManager.Instance.GetBlueprint(prototype.prototype.poolIndex);
	}

	/// <summary>
	/// Create a new entity based on the prototype
	/// </summary>
	/// <param name="prototype"></param>
	/// <param name="screenType">ScreenType for the new entity</param>
	/// <returns></returns>
	public GameEntity CreateFromPrototype(GameEntity prototype)
	{
		var game = _contexts.game;
		Assert.IsNotNull(prototype, "Cannot create an entity from a null prototype");
		Assert.IsTrue(prototype.hasPrototype, "Entity passed in is not a prototype");
		var newGO = ObjectPoolManager.Instance.GetPooledObject(prototype.prototype.poolIndex);
		if (newGO == null)
		{
			return null;
		}
		var newEntity = game.CreateEntity();
		var ignoreTypes = new List<System.Type> {
			typeof(PrototypeComponent)
		};
		EntitySupport.CopyComponentsToFrom(newEntity, prototype, ignoreTypes);

		Assert.IsFalse(prototype.hasView, "Prototype " + prototype.ToString() + " has a view! That shouldn't happen.");

		var name = ObjectPoolManager.Instance.GetName(prototype.prototype.poolIndex);
		newGO.name = Regex.Replace(name, "ID: [0-9]*", "ID: " + newEntity.id.value);
		Services.Instance.View.Create(newEntity, newGO);

		newEntity.AddFromPrototype(prototype.id.value);

		return newEntity;
	}

	#endregion // Interface Methods
}