﻿using System;
using Entitas;
using System.Collections.Generic;

public static class EntitySupport
{

	public static void CopyComponentsToFrom<T>(T target, T source) where T : IEntity
	{
		CopyComponentsToFrom(target, source, new List<Type>());
	}

	public static void CopyComponentsToFrom<T>(T target, T source, List<Type> ignoreTypes) where T : IEntity
	{
		ignoreTypes.Add(typeof(IdComponent));
		foreach (var c in source.GetComponents())
		{
			if (ignoreTypes.Contains(c.GetType()))
			{
				continue;
			}
			AddComponentToEntity(target, c);
		}
	}

	public static void AddComponentToEntity(IEntity e, IComponent c)
	{
		var index = GetIndexOfComponent(e, c);
		if (index >= 0 && !e.HasComponent(index))
		{
			var fields = c.GetType().GetFields();
			var newComponent = e.CreateComponent(index, c.GetType());
			foreach (var field in fields)
			{
				field.SetValue(newComponent, field.GetValue(c));
			}
			e.AddComponent(index, newComponent);
		}
	}

	public static void RemoveMatchingComponents<T>(T target, T source) where T : IEntity
	{
		RemoveMatchingComponents(target, source, new List<Type>());
	}

	public static void RemoveMatchingComponents<T>(T target, T source, List<Type> ignoreTypes) where T : IEntity
	{
		ignoreTypes.Add(typeof(IdComponent));
		ignoreTypes.Add(typeof(ActiveComponent));
		foreach (var c in source.GetComponents())
		{
			if (ignoreTypes.Contains(c.GetType()))
			{
				continue;
			}
			RemoveComponentFromEntity(target, c);
		}
	}

	public static void RemoveComponentFromEntity(IEntity e, IComponent c)
	{
		var index = GetIndexOfComponent(e, c);
		if (index >= 0 && e.HasComponent(index))
		{
			e.RemoveComponent(index);
		}
	}

	public static int GetIndexOfComponent(IEntity e, IComponent c)
	{
		var index = -1;
		var types = e.contextInfo.componentTypes;
		for (var i = 0; i < types.Length; i++)
		{
			if (types[i] == c.GetType())
			{
				index = i;
				break;
			}
		}
		return index;
	}
}