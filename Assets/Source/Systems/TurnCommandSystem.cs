﻿using Entitas;

public class TurnCommandSystem : IExecuteSystem
{

	readonly IGroup<CommandEntity> _Commands;
	readonly IGroup<GameEntity> _OldTurns;
	readonly GameContext _Game;

	public TurnCommandSystem(Contexts contexts)
	{
		var matcher = CommandMatcher
			.AllOf(
				CommandMatcher.Target
				, CommandMatcher.Turn);
		_Commands = contexts.command.GetGroup(matcher);
		_Game = contexts.game;
		_OldTurns = contexts.game.GetGroup(GameMatcher.Turn);
	}

	public void Execute()
	{
		foreach (var entity in _OldTurns.GetEntities())
		{
			entity.RemoveTurn();
		}
		foreach (var command in _Commands.GetEntities())
		{
			var target = _Game.GetEntityWithId(command.target.id);
			if (target.hasTurn)
			{
				target.turn.direction += command.turn.direction;
			}
			else
			{
				target.AddTurn(command.turn.direction);
			}
		}
	}
}