﻿using Entitas;

public class DestroyCommandsSystem : IExecuteSystem
{

	readonly CommandContext _Command;

	public DestroyCommandsSystem(Contexts contexts)
	{
		_Command = contexts.command;
	}

	public void Execute()
	{
		_Command.DestroyAllEntities();
	}
}
