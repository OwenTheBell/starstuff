﻿using Entitas;

public class VelocitySystem : IExecuteSystem
{

	readonly GameContext _Game;
	readonly IGroup<GameEntity> _Entities;

	public VelocitySystem(Contexts contexts)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Velocity,
				GameMatcher.View
			);
		_Entities = contexts.game.GetGroup(matcher);
		_Game = contexts.game;
	}

	public void Execute()
	{
		foreach (var e in _Entities.GetEntities())
		{
			var position = e.view.value.Position;
			position += e.velocity.value * _Game.time.Tick;
			e.view.value.Position = position;
		}
	}
}