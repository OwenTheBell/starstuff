﻿using Entitas;

/// <summary>
/// Accelerate commands are applied to the Accelerate component on target so that
/// other systems can know what cummulative direction this entity is going to accelerate
/// in this frame
/// </summary>
public class AccelerationCommandSystem : IExecuteSystem
{

	readonly IGroup<CommandEntity> _Commands;
	readonly IGroup<GameEntity> _OldAccelerates;
	readonly GameContext _Game;

	public AccelerationCommandSystem(Contexts contexts)
	{
		_Game = contexts.game;
		var matcher = CommandMatcher
			.AllOf(
				CommandMatcher.Target,
				CommandMatcher.Accelerate
			);
		_Commands = contexts.command.GetGroup(matcher);
		_OldAccelerates = contexts.game.GetGroup(GameMatcher.Accelerate);
	}

	public void Execute()
	{
		foreach (var entity in _OldAccelerates.GetEntities())
		{
			entity.RemoveAccelerate();
			if (entity.hasAccelerateCommandsThisFrame)
			{
				entity.RemoveAccelerateCommandsThisFrame();
			}
		}
		foreach (var command in _Commands.GetEntities())
		{
			var target = _Game.GetEntityWithId(command.target.id);
			var acceleration = command.accelerate.value;
			if (target.hasAccelerate)
			{
				var thisFrame = target.accelerateCommandsThisFrame.value;
				var factor = thisFrame / (thisFrame + 1f);
				var averagedAcceleration = acceleration / (thisFrame + 1f);
				target.ReplaceAccelerate(target.accelerate.value * factor + averagedAcceleration);
				target.ReplaceAccelerateCommandsThisFrame(thisFrame + 1);
			}
			else
			{
				target.AddAccelerate(acceleration);
				target.AddAccelerateCommandsThisFrame(1);
			}
		}
	}
}