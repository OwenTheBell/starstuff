﻿using Entitas;

public class DecreaseAICooldownSystem : IExecuteSystem
{
	readonly GameContext _Game;
	readonly IGroup<AIEntity> _Cooldowns;

	public DecreaseAICooldownSystem(Contexts contexts)
	{
		_Game = contexts.game;
		_Cooldowns = contexts.aI.GetGroup(AIMatcher.Cooldown);
	}

	public void Execute()
	{
		foreach (var e in _Cooldowns.GetEntities())
		{
			e.cooldown.remaining -= _Game.time.Tick;
			if (e.cooldown.remaining <= 0f)
			{
				e.RemoveCooldown();
			}
		}
	}
}
