﻿using Entitas;

public class AccelerateInputSystem : IExecuteSystem
{

	readonly Contexts _Contexts;
	readonly IGroup<InputEntity> _Inputs;

	public AccelerateInputSystem(Contexts contexts)
	{
		_Contexts = contexts;
		var matcher = InputMatcher
			.AllOf(
				InputMatcher.Target
				, InputMatcher.Accelerate
			);
		_Inputs = contexts.input.GetGroup(matcher);
	}

	public void Execute()
	{
		foreach (var input in _Inputs.GetEntities())
		{
			var target = _Contexts.game.GetEntityWithId(input.target.id);
			if (target.hasAcceleration)
			{
				var command = _Contexts.command.CreateEntity();
				command.AddTarget(target.id.value);
				var acceleration = input.accelerate.value
										* target.acceleration.value
										* _Contexts.game.time.Tick;
				if (target.hasAccelerationAmp)
				{
					acceleration *= target.accelerationAmp.value;
				}
				command.AddAccelerate(acceleration);
			}
		}
	}
}