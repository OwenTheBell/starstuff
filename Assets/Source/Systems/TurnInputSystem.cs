﻿using System.Collections.Generic;
using Entitas;

public class TurnInputSystem : ReactiveSystem<InputEntity>
{

	readonly GameContext _Game;
	readonly CommandContext _Command;

	public TurnInputSystem(Contexts contexts) : base(contexts.input)
	{
		_Game = contexts.game;
		_Command = contexts.command;
	}

	protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
	{
		var matcher = InputMatcher
			.AllOf(
				InputMatcher.Target
				, InputMatcher.Turn
			);
		return context.CreateCollector(matcher);
	}

	protected override bool Filter(InputEntity entity)
	{
		return entity.hasTarget;
	}

	protected override void Execute(List<InputEntity> inputs)
	{
		var tick = _Game.time.Tick;
		foreach (var input in inputs)
		{
			var target = _Game.GetEntityWithId(input.target.id);
			if (target.hasAngularAcceleration && target.hasAngularVelocity)
			{
				var command = _Command.CreateEntity();
				command.AddTarget(target.id.value);
				command.AddTurn(input.turn.direction * target.angularAcceleration.value * tick);
			}
		}
	}
}