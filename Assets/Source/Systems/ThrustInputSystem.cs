﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class ThrustInputSystem : ReactiveSystem<InputEntity>
{
	readonly Contexts _Contexts;

	public ThrustInputSystem(Contexts contexts) : base(contexts.input)
	{
		_Contexts = contexts;
	}

	protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
	{
		var matcher = InputMatcher
			.AllOf(
				InputMatcher.Target,
				InputMatcher.Thrust);
		return context.CreateCollector(matcher);
	}

	protected override bool Filter(InputEntity input)
	{
		if (!input.hasTarget)
		{
			return false;
		}
		var target = _Contexts.game.GetEntityWithId(input.target.id);
		return target.hasFacing && target.hasAcceleration && target.hasVelocity;
	}

	protected override void Execute(List<InputEntity> inputs)
	{
		foreach (var input in inputs)
		{
			var target = _Contexts.game.GetEntityWithId(input.target.id);
			var tick = _Contexts.game.time.Tick;
			var rad = Mathf.Deg2Rad * target.facing.value;
			var direction = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad));
			input.AddAccelerate(direction);
		}
	}
}