﻿using System.Collections.Generic;
using Entitas;

public class ScalingMaximumVelocitySystem : ReactiveSystem<GameEntity>
{
	readonly IGroup<GameEntity> _Entities;

	public ScalingMaximumVelocitySystem(Contexts contexts) : base(contexts.game)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.ScalingMaximumVelocity
				, GameMatcher.MaximumVelocity);
		_Entities = contexts.game.GetGroup(matcher);
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Player
				, GameMatcher.MaximumVelocity
			);
		return context.CreateCollector(matcher);
	}

	protected override bool Filter(GameEntity entity)
	{
		return true;
	}

	protected override void Execute(List<GameEntity> entities)
	{
		foreach (var e in _Entities.GetEntities())
		{
			e.ReplaceMaximumVelocity(entities[0].maximumVelocity.value * e.scalingMaximumVelocity.percent);
		}
	}
}
