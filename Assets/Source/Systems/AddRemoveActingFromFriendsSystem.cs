﻿using System.Collections.Generic;
using Entitas;

public class AddRemoveActingFromFriendsSystem : ReactiveSystem<AIEntity> 
{

	readonly GameContext _Game;

	public AddRemoveActingFromFriendsSystem(Contexts contexts) : base(contexts.aI)
	{
		_Game = contexts.game;
	}

	protected override ICollector<AIEntity> GetTrigger(IContext<AIEntity> context)
	{
		return context.CreateCollector(AIMatcher.Acting.AddedOrRemoved());
	}

	protected override bool Filter(AIEntity entity)
	{
		return entity.hasAgent;
	}

	protected override void Execute(List<AIEntity> entities)
	{
		foreach (var e in entities)
		{
			var agent = _Game.GetEntityWithId(e.agent.id);
			agent.isActing = e.isActing;
		}
	}
}
