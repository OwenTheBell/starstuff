﻿using System.Collections.Generic;
using Entitas;

public class FilterInputForLockoutControlSystem : ReactiveSystem<InputEntity>
{

	readonly GameContext _Game;

	public FilterInputForLockoutControlSystem(Contexts contexts) : base(contexts.input)
	{
		_Game = contexts.game;
	}

	protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
	{
		return context.CreateCollector(InputMatcher.Target);
	}

	protected override bool Filter(InputEntity entity)
	{
		var target = _Game.GetEntityWithId(entity.target.id);
		return target == null || target.isLockoutControl;
	}

	protected override void Execute(List<InputEntity> entities)
	{
		entities.Act(e => e.RemoveTarget());
	}
}
