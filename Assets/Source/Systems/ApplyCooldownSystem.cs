﻿using System.Collections.Generic;
using Entitas;

public class ApplyCooldownSystem : ReactiveSystem<AIEntity> 
{

	readonly AIContext _AI;

	public ApplyCooldownSystem(Contexts contexts) : base(contexts.aI)
	{
		_AI = contexts.aI;
	}

	protected override ICollector<AIEntity> GetTrigger(IContext<AIEntity> context)
	{
		return context.CreateCollector(AIMatcher.Acting.Removed());
	}

	protected override bool Filter(AIEntity entity)
	{
		return !entity.isActing && entity.hasAgent && _AI.hasActingCooldown;
	}

	protected override void Execute(List<AIEntity> entities)
	{
		foreach (var e in entities)
		{
			e.AddCooldown(_AI.actingCooldown.duration);
		}
	}
}
