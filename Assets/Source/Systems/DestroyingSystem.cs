﻿using Entitas;

public class DestroyingSystem : ICleanupSystem
{

	readonly IGroup<GameEntity> _ToDestroy;
	readonly IGroup<AIEntity> _AiToDestroy;

	public DestroyingSystem(Contexts contexts)
	{
		_ToDestroy = contexts.game.GetGroup(GameMatcher.Destroying);
		_AiToDestroy = contexts.aI.GetGroup(AIMatcher.Destroy);
	}

	public void Cleanup()
	{
		foreach (var e in _ToDestroy.GetEntities())
		{
			if (e.hasView)
			{
				e.view.value.DestroyView();
			}
			e.Destroy();
		}
		foreach (var e in _AiToDestroy.GetEntities())
		{
			e.Destroy();
		}
	}
}