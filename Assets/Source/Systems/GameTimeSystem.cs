﻿using Entitas;
using UnityEngine;

public class GameTimeSystem : IInitializeSystem, IExecuteSystem
{

	readonly GameContext _Game;

	public GameTimeSystem(Contexts contexts)
	{
		_Game = contexts.game;
	}

	public void Initialize()
	{
		var tick = _Game.CreateEntity();
		_Game.SetTime(0f, 0f, 1f, 0f, 0f);
	}

	public void Execute()
	{
		if (!_Game.hasTime)
		{
			return;
		}

		var tick = 0f;
		var fixedTick = 0f;
		var scale = _Game.time.Scale;
		var unpausedTick = Time.deltaTime;
		if (!_Game.isPaused)
		{
			tick = Time.deltaTime * _Game.time.Scale;
			fixedTick = Time.fixedDeltaTime;
		}
		else
		{
			tick = 0f;
			fixedTick = 0f;
		}
		var time = _Game.time.Time + tick;
		_Game.ReplaceTime(tick, time, scale, fixedTick, unpausedTick);

	}
}
