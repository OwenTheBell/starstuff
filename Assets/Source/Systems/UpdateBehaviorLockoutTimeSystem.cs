﻿using Entitas;

public class UpdateBehaviorLockoutTimeSystem : IExecuteSystem
{

	readonly Contexts _Contexts;

	public UpdateBehaviorLockoutTimeSystem(Contexts contexts)
	{
		_Contexts = contexts;
	}

	public void Execute()
	{
		if (_Contexts.aI.behaviorLockoutTime.remaining > 0f)
		{
			_Contexts.aI.behaviorLockoutTime.remaining -= _Contexts.game.time.Tick;
		}
	}
}
