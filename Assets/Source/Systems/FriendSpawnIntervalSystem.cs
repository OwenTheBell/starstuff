﻿using Entitas;

public class FriendSpawnIntervalSystem : IExecuteSystem
{

	const float SPAWN_RANGE = 60f;
	readonly Contexts _Contexts;
	readonly IGroup<GameEntity> _Entities;
	readonly IGroup<GameEntity> _Friends;

	public FriendSpawnIntervalSystem(Contexts contexts)
	{
		_Contexts = contexts;
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.SpawnInterval
				, GameMatcher.Accelerate
				, GameMatcher.View
				, GameMatcher.Velocity
			);
		_Entities = contexts.game.GetGroup(matcher);
		matcher = GameMatcher
			.AllOf(
				GameMatcher.Friend
				, GameMatcher.View);
		_Friends = contexts.game.GetGroup(matcher);
	}

	public void Execute()
	{
		if (!_Contexts.game.hasTime)
		{
			return;
		}
		foreach (var e in _Entities.GetEntities())
		{
			e.spawnInterval.split -= _Contexts.game.time.Tick;	
			if (e.spawnInterval.split <= 0f)
			{
				e.ReplaceSpawnInterval(e.spawnInterval.duration, e.spawnInterval.duration);
				var position = e.view.value.Position;
				var direction = -e.velocity.value.normalized;
				var friend = Services.Instance.FriendSpawner.Spawn(_Contexts, position + direction * SPAWN_RANGE);
				if (friend != null)
				{
					friend.ReplaceVelocity(e.velocity.value);
				}
			}
		}
	}
}