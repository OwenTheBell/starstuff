﻿using Entitas;

public class AngularVelocitySystem : IExecuteSystem
{

	readonly GameContext _Game;
	readonly IGroup<GameEntity> _Entities;

	public AngularVelocitySystem(Contexts contexts)
	{
		_Game = contexts.game;
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.AngularVelocity
				, GameMatcher.Facing);
		_Entities = contexts.game.GetGroup(matcher);
	}

	public void Execute()
	{
		var tick = _Game.time.Tick;
		foreach (var e in _Entities.GetEntities())
		{
			var facing = e.facing.value;
			e.ReplaceFacing(facing + e.angularVelocity.value * tick);
		}
	}
}