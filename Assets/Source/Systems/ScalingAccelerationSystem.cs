﻿using System.Collections.Generic;
using Entitas;

public class ScalingAccelerationSystem : ReactiveSystem<GameEntity>
{

	readonly IGroup<GameEntity> _Entities;

	public ScalingAccelerationSystem(Contexts contexts) : base(contexts.game)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.ScalingAcceleration
				, GameMatcher.Acceleration);
		_Entities = contexts.game.GetGroup(matcher);
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Player
				, GameMatcher.Acceleration
		);
		return context.CreateCollector(matcher);
	}

	protected override bool Filter(GameEntity entity)
	{
		return true;
	}

	protected override void Execute(List<GameEntity> entities)
	{
		foreach (var e in _Entities.GetEntities())
		{
			e.ReplaceAcceleration(entities[0].acceleration.value * e.scalingAcceleration.percent);
		}
	}
}