﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entitas;

public class LockoutPlayerControlAtEndSystem : ReactiveSystem<GameEntity>
{

	readonly IGroup<GameEntity> _Players;
	readonly IGroup<GameEntity> _Friends;

	public LockoutPlayerControlAtEndSystem(Contexts contexts) : base(contexts.game)
	{
		_Players = contexts.game.GetGroup(GameMatcher.Player);
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Friend
				, GameMatcher.View);
		_Friends = contexts.game.GetGroup(matcher);
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Friend
				, GameMatcher.View);
		return context.CreateCollector(matcher.Removed());
	}

	protected override bool Filter(GameEntity entity)
	{
		return _Friends.count == 0;
	}

	protected override void Execute(List<GameEntity> entities)
	{
		_Players.GetSingleEntity().isLockoutControl = true;
	}
}
