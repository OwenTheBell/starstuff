﻿using Entitas;
using UnityEngine;

public class AngularVelocityDampingSystem : IExecuteSystem
{

	readonly GameContext _Game;
	readonly IGroup<GameEntity> _Entities;

	public AngularVelocityDampingSystem(Contexts contexts)
	{
		_Game = contexts.game;
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.AngularVelocity
				, GameMatcher.AngularVelocityDamping
			);
		_Entities = contexts.game.GetGroup(matcher);
	}

	public void Execute()
	{
		foreach (var e in _Entities.GetEntities())
		{
			if ((e.hasTurn && e.turn.direction / e.angularVelocity.value < 0f)
				|| (e.hasMaximumAngularVelocity
					&& Mathf.Abs(e.angularVelocity.value) > e.maximumAngularVelocity.value)
				|| !e.hasTurn)
			{
				var percent = Mathf.Clamp01(_Game.time.Tick / e.angularVelocityDamping.value);
				var damp = e.angularVelocity.value * percent;
				e.angularVelocity.value -= damp;
			}
		}
	}
}
