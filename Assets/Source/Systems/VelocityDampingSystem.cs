﻿using Entitas;
using UnityEngine;

public class VelocityDampingSystem : IExecuteSystem
{

	readonly GameContext _Game;
	readonly IGroup<GameEntity> _Entities;

	public VelocityDampingSystem(Contexts contexts)
	{
		_Game = contexts.game;
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Velocity
				, GameMatcher.VelocityDamping
				, GameMatcher.Accelerate
			);
		_Entities = contexts.game.GetGroup(matcher);
	}

	public void Execute()
	{
		foreach (var e in _Entities.GetEntities())
		{
			var percent = 0f;
			if (e.hasMaximumVelocity && e.maximumVelocity.value < e.velocity.value.magnitude)
			{
				percent = 1f;
			}
			else if (e.hasAccelerate)
			{
				var dot = Vector3.Dot(e.accelerate.value.normalized, e.velocity.value.normalized);
				percent = 1f - (dot + 1f) / 2f;
			}
			else
			{
				percent = 1f;
			}
			var damp = e.velocity.value * (e.velocityDamping.value * percent);
			e.ReplaceVelocity(e.velocity.value - damp * _Game.time.Tick);
		}
	}
}