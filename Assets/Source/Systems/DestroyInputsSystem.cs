﻿using Entitas;

public class DestroyInputsSystem : IExecuteSystem
{

	readonly InputContext _Input;
	
	public DestroyInputsSystem(Contexts contexts)
	{
		_Input = contexts.input;
	}

	public void Execute()
	{
		_Input.DestroyAllEntities();
	}
}
