﻿using Entitas;
using UnityEngine;

public class MaximumAngularVelocitySystem : IExecuteSystem
{

	readonly IGroup<GameEntity> _Entities;

	public  MaximumAngularVelocitySystem(Contexts contexts)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.AngularVelocity
				, GameMatcher.MaximumAngularVelocity
			)
			.NoneOf(
				GameMatcher.AngularVelocityDamping
			);
		_Entities = contexts.game.GetGroup(matcher);
	}

	public void Execute()
	{
		foreach (var e in _Entities.GetEntities())
		{
			e.angularVelocity.value = Mathf.Clamp(e.angularVelocity.value
													, -e.maximumAngularVelocity.value
													, e.maximumAngularVelocity.value);
		}
	}
}
