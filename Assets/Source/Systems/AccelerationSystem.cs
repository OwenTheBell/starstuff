﻿using Entitas;

public class AccelerationSystem : IExecuteSystem
{

	readonly IGroup<GameEntity> _Entities;

	public AccelerationSystem(Contexts contexts)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Velocity
				, GameMatcher.Accelerate
			);
		_Entities = contexts.game.GetGroup(matcher);
	}

	public void Execute()
	{
		foreach (var e in _Entities.GetEntities())
		{
			e.ReplaceVelocity(e.velocity.value + e.accelerate.value);
		}
	}
}