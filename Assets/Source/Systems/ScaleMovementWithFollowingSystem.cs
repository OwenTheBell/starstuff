﻿using System.Collections.Generic;
using Entitas;

public class ScaleMovementWithFollowingSystem : ReactiveSystem<GameEntity>
{

	readonly IGroup<GameEntity> _AdjustingEntities;

	public ScaleMovementWithFollowingSystem(Contexts contexts) : base(contexts.game)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.MovementAdjust
				, GameMatcher.Acceleration
				, GameMatcher.MaximumVelocity
			);
		_AdjustingEntities = contexts.game.GetGroup(matcher);
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Following
			);
		return context.CreateCollector(matcher);
	}

	protected override bool Filter(GameEntity entity)
	{
		return true;
	}

	protected override void Execute(List<GameEntity> entities)
	{
		foreach (var e in entities)
		{
			if (e.isFollowing)
			{
				Adjust(1);
			}
			else
			{
				Adjust(-1);
			}
		}
	}

	void Adjust(int direction)
	{
		foreach (var e in _AdjustingEntities.GetEntities())
		{
			e.ReplaceAcceleration(e.acceleration.value + e.movementAdjust.Acceleration * direction);
			e.ReplaceMaximumVelocity(e.maximumVelocity.value + e.movementAdjust.MaxVelocity * direction);
		}
	}
}