﻿using Entitas;

public class TurnSystem : IExecuteSystem
{
	readonly IGroup<GameEntity> _Entities;

	public TurnSystem(Contexts contexts)
	{
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Turn
				, GameMatcher.AngularVelocity
			);
		_Entities = contexts.game.GetGroup(matcher);
	}

	public void Execute()
	{
		foreach (var e in _Entities.GetEntities())
		{
			e.ReplaceAngularVelocity(e.angularVelocity.value + e.turn.direction);
		}
	}
}
