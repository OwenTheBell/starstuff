﻿using Entitas;
using UnityEngine;

public class MaximumVelocitySystem : IExecuteSystem
{

	readonly GameContext _Game;
	readonly IGroup<GameEntity> _Entities;

	public MaximumVelocitySystem(Contexts contexts)
	{
		_Game = contexts.game;
		var matcher = GameMatcher
			.AllOf(
				GameMatcher.Velocity
				, GameMatcher.MaximumVelocity
			)
			.NoneOf(
				GameMatcher.VelocityDamping
			);
		_Entities = contexts.game.GetGroup(matcher);
	}

	public void Execute()
	{
		foreach (var e in _Entities.GetEntities())
		{
			var magnitude = e.velocity.value.magnitude;
			var maxVelocity = e.maximumVelocity.value;
			if (e.hasAdjustMaximumVelocity)
			{
				maxVelocity *= e.adjustMaximumVelocity.percent;
			}
			if (magnitude > maxVelocity)
			{
				var newVelocity = Vector3.ClampMagnitude(e.velocity.value, maxVelocity);
				e.ReplaceVelocity(newVelocity);
			}
		}
	}
}