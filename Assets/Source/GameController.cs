﻿using UnityEngine;
using Entitas;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Assertions;
using Common.ObjectPooling;
using Sirenix.OdinInspector;

/// <summary>
/// Controls Entitas during mash as well as SceneControllers for building mash
/// </summary>
public class GameController : MonoBehaviour, IGameController {

    #region IGameController Properties

    public Contexts AllContexts { get; private set; }
    public SystemFeature Systems { get; private set; }

    #endregion // IGameController Properties


    #region Unity Lifecycle Methods

    private void Awake() {
        AllContexts = Contexts.sharedInstance;

        foreach (var c in AllContexts.allContexts) {
			c.OnEntityCreated += AddId;
		}

        /* initialize all services */
        new Services(AllContexts);
        Services.Instance.GameController = this;

        Systems = new SystemFeature("Systems");
		Systems.Add(new GameTimeSystem(AllContexts));
		Systems.Add(new UpdateBehaviorLockoutTimeSystem(AllContexts));
		Systems.Add(new ApplyCooldownSystem(AllContexts));
		Systems.Add(new DecreaseAICooldownSystem(AllContexts));
		Systems.Add(new InputFeature(AllContexts));
		Systems.Add(new CommandFeature(AllContexts));
		Systems.Add(new GameFeature(AllContexts));
		Systems.Add(new GameEventSystems(AllContexts));
		Systems.Add(new DestroyingSystem(AllContexts));
    }

    public void Start() {
        // this is here so the game can be launched from a debugging mash without
        // needing to go through the menus. At game start, since there are no controllers
        // this will do nothing
        Begin();
    }

    void Update() {
        Systems.Execute();
        Systems.Cleanup();
    }

    private void FixedUpdate() {
        Systems.FixedUpdate();
    }

    private void OnDestroy() {
        foreach (var c in AllContexts.allContexts) {
			c.OnEntityCreated -= AddId;
		}
    }

    #endregion // Unity Lifecycle Methods


    #region IGameController Methods

    /// <summary>
    /// Go through GenreManagers to setup the mash. Then start the mash running.
    /// </summary>
    public void Begin() {
        Systems.Initialize();
        Systems.ActivateReactiveSystems();

		foreach (var setup in FindObjectsOfType<SetupEntity>())
		{
			setup.Setup(AllContexts);
		}
    }
        
    /// <summary>
    /// Reset the current mash to a start point
    /// </summary>
    public void Reset() {
    }

    /// <summary>
    /// Cleanup everything at the end of a mash
    /// </summary>
    public void TearDown() {
        Systems.TearDown();
        ObjectPoolManager.Instance.Empty();
    }

	#endregion // IGameController Methods

	#region New Entity Delegates

	void AddId(IContext c, IEntity e)
	{
		if (!(e as IIdEntity).hasId)
		{
			(e as IIdEntity).AddId(e.creationIndex);
		}
	}

	#endregion // New Entity Delegates
}