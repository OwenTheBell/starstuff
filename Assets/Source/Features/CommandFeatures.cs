﻿public class CommandFeature : SystemFeature
{
	public CommandFeature(Contexts contexts) : base("Command Feature")
	{
		Add(new TurnCommandSystem(contexts));
		Add(new AccelerationCommandSystem(contexts));

		Add(new DestroyCommandsSystem(contexts));
	}
}
