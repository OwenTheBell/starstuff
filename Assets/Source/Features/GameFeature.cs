﻿public class GameFeature : SystemFeature
{
	public GameFeature(Contexts contexts) : base("System Feature")
	{
		Add(new ScaleMovementWithFollowingSystem(contexts));
		Add(new ScalingAccelerationSystem(contexts));
		Add(new ScalingMaximumVelocitySystem(contexts));
		Add(new VelocityDampingSystem(contexts));
		Add(new AngularVelocityDampingSystem(contexts));
		Add(new AccelerationSystem(contexts));
		Add(new TurnSystem(contexts));
		Add(new MaximumVelocitySystem(contexts));
		Add(new MaximumAngularVelocitySystem(contexts));
		Add(new AngularVelocitySystem(contexts));
		Add(new VelocitySystem(contexts));
		Add(new FriendSpawnIntervalSystem(contexts));
		Add(new AddRemoveActingFromFriendsSystem(contexts));
		Add(new LockoutPlayerControlAtEndSystem(contexts));
	}
}