﻿public class InputFeature : SystemFeature
{
	public InputFeature (Contexts contexts) :  base("Input Feature")
	{
		Add(new FilterInputForLockoutControlSystem(contexts));

		Add(new TurnInputSystem(contexts));
		Add(new ThrustInputSystem(contexts));
		Add(new AccelerateInputSystem(contexts));

		Add(new DestroyInputsSystem(contexts));
	}
}